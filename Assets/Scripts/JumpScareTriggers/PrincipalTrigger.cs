using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrincipalTrigger : MonoBehaviour
{
    public GameObject mannequin;
    public AudioClip jumpscare;
    private AudioSource jumpscareSource;

    private void Start()
    {
        mannequin.SetActive(false);
        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            jumpscareSource = AudioManager.Instance.PlaySound(jumpscare);
            mannequin.SetActive(true);
            Invoke("Destroy", jumpscare.length);
            Invoke("Delete", 0.9f);
        }
    }

    private void Delete()
    {
        mannequin.SetActive(false);
    }

    private void Destroy()
    {
        AudioManager.Instance.StopSound(jumpscareSource);
        Destroy(gameObject);
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current)
    {
        if (current.gameStage == GameManager.GameStage.SecondStage)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
