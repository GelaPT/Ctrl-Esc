using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mannequin : MonoBehaviour
{
    private void Start()
    {
        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.FirstStage)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }
    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current)
    {
        if (current.gameStage != GameManager.GameStage.FirstStage)
        {
            gameObject.SetActive(false);
        }
    }
}
