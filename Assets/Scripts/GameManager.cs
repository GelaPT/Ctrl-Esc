using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// Evento - Mudança de Máquina de Estados
/// </summary>
[System.Serializable]
public class EventGameState : UnityEvent<GameManager.GameStates, GameManager.GameStates> { }

public class GameManager : Singleton<GameManager> {

    #region Game State

    public GameStates startGameStates;
    public enum GameState { Null, PreGame, Running, Paused };
    public enum GameStage { FirstCut, FirstStage, SecondStage, LastCut, ThirdStage };
    [System.Serializable]
    public struct GameStates {
        public GameState gameState;
        public GameStage gameStage;

        public GameStates(GameState gameState, GameStage gameStage) {
            this.gameStage = gameStage;
            this.gameState = gameState;
        }
    }
    public GameStates CurrentGameState { get; private set; }
    public GameStates PreviousGameState { get; private set; }
    
    public EventGameState onGameStateChanged;

    public void UpdateState(GameStates newGameState) {
        PreviousGameState = CurrentGameState;
        CurrentGameState = newGameState;

        onGameStateChanged.Invoke(PreviousGameState, CurrentGameState);
    }

    private void GameStateAwake() {
        CurrentGameState = new GameStates(GameState.Null, GameStage.FirstCut);
        CurrentGameState = new GameStates(GameState.PreGame, CurrentGameState.gameStage);
    }

    #endregion Game State


    #region Managers

    public GameObject[] managerPrefabs;
    private List<GameObject> instantiatedManagerPrefabs;

    private void ManagersAwake() {
        instantiatedManagerPrefabs = new List<GameObject>();

        InstantiateManagerPrefabs();
    }

    private void ManagersOnDestroy() {
        for(var i = 0; i < instantiatedManagerPrefabs.Count; i++) Destroy(instantiatedManagerPrefabs[i]);
        instantiatedManagerPrefabs.Clear();
    }

    private void InstantiateManagerPrefabs() {
        GameObject prefabInstance;
        for (var i = 0; i < managerPrefabs.Length; i++) {
            prefabInstance = Instantiate(managerPrefabs[i]);
            instantiatedManagerPrefabs.Add(prefabInstance);
        }
    }

    #endregion Managers


    #region Utils
    public SaveData saveData;
    static int currentScene = 0;

    public void Pause() {
        Time.timeScale = 0;
        UpdateState(new GameStates(GameState.Paused, CurrentGameState.gameStage));
    }

    public void UnPause() {
        Time.timeScale = 1;
        UpdateState(new GameStates(GameState.Running, CurrentGameState.gameStage));
    }

    public static void LoadScene(int scene) {
        SceneManager.UnloadSceneAsync(currentScene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        AudioManager.Instance.ResetAudio();
        currentScene = scene;
    }

    private void MainMenu() {
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        currentScene = 1;
    }

    public void StartNewGame() {
        saveData = new SaveData(1, new Vector3(-13, 1.5f, 38), Quaternion.Euler(0, 180, 0), false, 0, new List<string>());
        SaveManager.SaveGame(saveData);
        LoadScene(2);
        UpdateState(new GameStates(GameState.Running, GameStage.FirstCut));
    }

    public void ContinueGame() {
        switch(saveData.stage) {
            case 1:
                UpdateState(new GameStates(GameState.Running, GameStage.FirstStage));
                break;
            case 2:
                UpdateState(new GameStates(GameState.Running, GameStage.SecondStage));
                break;
            case 3:
                UpdateState(new GameStates(GameState.Running, GameStage.ThirdStage));
                break;
        }
        LoadScene(3);
    }
    #endregion


    #region MonoBehaviour

    protected override void Awake() {
        base.Awake();
        saveData = SaveManager.LoadGameSave();

        DontDestroyOnLoad(gameObject);
        ManagersAwake();
        
        GameStateAwake();
    }

    private void Start() {
        UpdateState(startGameStates);
        if(startGameStates.gameState == GameState.PreGame) {
            MainMenu();
        }
    }

    private void Update() {
        if(CurrentGameState.gameState != GameState.PreGame && CurrentGameState.gameStage != GameStage.FirstCut && CurrentGameState.gameStage != GameStage.LastCut) {
            if(Input.GetKeyDown(KeyCode.Escape)) {
                if(TaskManager.Instance.IsTask) return;
                if(CurrentGameState.gameState == GameState.Paused) UnPause();
                else Pause();
            }
        }
    }

    protected override void OnDestroy() {
        base.OnDestroy();
        ManagersOnDestroy();
    }

    #endregion MonoBehaviour

}