using UnityEngine;

public class Flashlight : MonoBehaviour {
    [SerializeField] private Light flashlight;
    private bool flashlightOn = false;

    [SerializeField] private AudioClip flashOn;
    [SerializeField] private AudioClip flashOff;

    private void Update() {
        if(Input.GetKeyDown(KeyCode.F)) {
            flashlightOn = !flashlightOn;
            GetComponent<Animator>().SetBool("isOn", flashlightOn);
            if (flashlightOn)
                AudioManager.Instance.PlaySound(flashOn);
            else
                AudioManager.Instance.PlaySound(flashOff);
        }
    }

    public void ToggleFlashlight() {
        flashlight.gameObject.SetActive(flashlightOn);
    }
}