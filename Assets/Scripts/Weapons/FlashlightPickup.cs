using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightPickup : MonoBehaviour
{
    [SerializeField] GameObject flashlight;
    public Light spotLight;
    public void EnablePickUp()
    {
        GetComponent<Outline>().enabled = true;
        spotLight.enabled = true;
    }
    public void PickUp()
    {
        flashlight.SetActive(true);
        MonologueManager.Instance.Notification();
        Destroy(gameObject);
    }
}
