using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardPlayBored : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<GuardScript>())
        {
            other.gameObject.GetComponent<Animator>().Play("Base Layer.Bored");
            other.gameObject.GetComponent<GuardScript>().agent.isStopped = true;
            Destroy(gameObject);
        }
    }
}
