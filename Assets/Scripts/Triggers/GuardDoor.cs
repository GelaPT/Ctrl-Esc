using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardDoor : MonoBehaviour
{
    
    void Start()
    {
        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage)
        {
            GetComponent<Door>().ForceClose();
        }
        else if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage)
        {
            GetComponent<Door>().ForceClose();
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current)
    {
        if (current.gameStage == GameManager.GameStage.SecondStage)
        {
            GetComponent<Door>().ForceClose();
        }
        if (current.gameStage == GameManager.GameStage.ThirdStage)
        {
            GetComponent<Door>().ForceClose();
        }
    }
}
