using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardEnterRoomScript : MonoBehaviour
{
    [SerializeField] private Door door;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<GuardScript>())
        {
            door.Open();
            Invoke("CloseDoor", 0.5f);
        }
    }

    private void CloseDoor()
    {
        door.Lock();
        Destroy(gameObject);
    }
}
