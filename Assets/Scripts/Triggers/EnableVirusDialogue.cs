using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableVirusDialogue : MonoBehaviour
{
    public VirusDialogueScript vds;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            vds.enabled = true;
            Destroy(gameObject);
        }
    }
}
