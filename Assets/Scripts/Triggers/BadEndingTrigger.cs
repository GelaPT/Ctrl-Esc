using UnityEngine;

public class BadEndingTrigger : MonoBehaviour
{
    public int i = 0;
    public Animator mannAnimator;
    public Animator animator;
    public GameObject virusdialoguedois;
    public GameObject player;
    public GameObject FPScamera;
    public HeadBob cameraHeadbob;
    public HeadBob clashlightHeadbob;
    [SerializeField] private ThirdStage thirdStage;
    public GameObject mannequin;
    public GameObject badendingMannequin;
    public GameObject virusDoor;
    public CeilingLamp1 viruslamp;
    public GameObject celphone;
    public GameObject flashlight;
    public GameObject endingPanel;
    public GameObject endingText;

    private void OnTriggerEnter(Collider other)
    {
        if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage) {
            thirdStage.EnableTriggers();
            Destroy(gameObject);
            return;
        }
        if(other.gameObject.tag == "Player")
        {
            player.GetComponent<CharacterController>().enabled = false;
            virusdialoguedois.SetActive(true);
            player.transform.position = new Vector3(-13.9650002f, 1.5f, 37.9399986f);
            player.transform.LookAt(virusdialoguedois.transform);
            FPScamera.transform.LookAt(virusdialoguedois.transform);
            player.GetComponent<PlayerScript>().canMove = false;
            FPScamera.GetComponent<MouseLook>().canLook = false;
            cameraHeadbob.enabled = false;
            clashlightHeadbob.enabled = false;
            virusDoor.SetActive(false);
            viruslamp.ForceTurnOff();
            celphone.SetActive(false);
            flashlight.SetActive(false);
        }
    }

    public void UpdateAnimator()
    {
        //player.transform.LookAt(mannequin.transform);
        badendingMannequin.SetActive(true);
        mannAnimator.SetInteger("State", i);
        animator.SetInteger("State", i);
    }

    public void EndGame()
    {
        virusdialoguedois.GetComponent<VirusDialogue2Script>().badendingcamera.SetActive(false);
        AudioManager.Instance.ResetAudio();
        endingText.SetActive(true);
        endingPanel.SetActive(true);
        Invoke("Credits", 10);
    }

    private void Credits()
    {
        AudioManager.Instance.ResetAudio();
        GameManager.LoadScene(4);
    }
}
