using UnityEngine;

public class GoodEnding : MonoBehaviour
{
    public Animator playerAnimator;
    public Camera playercamera;
    public CeilingLamp1[] lamps;
    public AudioClip clip;
    public Door door;
    public GameObject mannequin;
    public GameObject celphone;
    public GameObject flashlight;
    public GameObject endingPanel;
    public GameObject endingText;
    public PlayerScript playerscript;
    public GameObject thatpaper;
    public CeilingLamp1 luzprincipal;

    private bool firsttime = true;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            luzprincipal.TurnAllOn();
            luzprincipal.TurnOn();
            door.ForceOpen();
            playercamera.gameObject.SetActive(true);
            mannequin.SetActive(true);
            playerAnimator.SetInteger("State", 1);
            celphone.SetActive(false);
            flashlight.SetActive(false);
            playerscript.canMove = false;
        }
    }

    public void Ending()
    {
        AudioManager.Instance.ResetAudio();
        playercamera.gameObject.SetActive(false);
        endingPanel.SetActive(true);
        endingText.SetActive(true);
        mannequin.SetActive(false);
        Invoke("End", 30f);
    }
    public void PlayClip()
    {
        if (firsttime)
        {
            AudioSource source = AudioManager.Instance.PlaySound(clip);
            firsttime = false;
        }
    }

    public void TurnAllOff()
    {
        thatpaper.SetActive(false);
        foreach (CeilingLamp1 lamp in lamps)
        {
            lamp.TurnAllOff();
        }
    }
    private void End()
    {
        Cursor.lockState = CursorLockMode.Confined;
        GameManager.LoadScene(4);
    }
}
