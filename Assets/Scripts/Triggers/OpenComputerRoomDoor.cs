using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenComputerRoomDoor : MonoBehaviour
{
    public AudioClip customAudioClip;
    public Door door;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            AudioManager.Instance.PlaySound(customAudioClip);
            if(door)
                door.ForceOpen();
            Destroy(gameObject);
        }
    }
}
