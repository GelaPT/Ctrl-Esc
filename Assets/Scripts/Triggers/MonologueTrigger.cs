using UnityEngine;

public class MonologueTrigger : MonoBehaviour
{
    public string personName;
    [TextArea(10, 30)] public string speech;
    public bool speak = false;
    public int audioMin, audioMax;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(personName != "")
                MonologueManager.Instance.ShowMonologue(personName, speech);
            if (speak)
                MonologueManager.Instance.SpeakMonologue(Random.Range(audioMin, audioMax));
            Destroy(gameObject);
        }
    }
}
