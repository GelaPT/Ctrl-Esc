using UnityEngine;
public class ThirdStage : MonoBehaviour
{
    private void Update() {
        if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage) {
            EnableTriggers();
        }
    }

    public GameObject[] triggers;
    public void EnableTriggers()
    {
        foreach(GameObject obj in triggers)
        {
            if(obj) obj.SetActive(true);
        }
    }
}
