using UnityEngine;
using UnityEngine.Video;

public class AfterFuseBox : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private Light computerLight;
    [SerializeField] private GameObject flashlight;
    [SerializeField] private GameObject piece;
    [SerializeField] private CeilingLamp1[] lamps;
    [SerializeField] private Light stairLight;
    [SerializeField] private GameObject[] objectsToActivateAfterFirst;
    [SerializeField] private GameObject[] objectsToActivateAfterSecond;
    [SerializeField] private AudioClip blackoutClip;
    [SerializeField] private GameObject guard;
    [SerializeField] private GameObject vp;
    private AudioSource blackoutSource;

    private void Start() {
        vp.SetActive(false);
        computerLight.enabled = false;
    }

    public void FuseBoxFirst()
    {
        MonologueManager.Instance.Notification();
        MonologueManager.Instance.ShowMonologue("player", "Oh, GREAT, the power went out. There should be a flashlight around here somewhere.");
        foreach (CeilingLamp1 lamp in lamps)
        {
            lamp.TurnAllOff();
        }
        foreach(GameObject obj in objectsToActivateAfterFirst)
        {
            obj.SetActive(true);
        }
        Destroy(stairLight);
        flashlight.GetComponent<FlashlightPickup>().EnablePickUp();
        piece.GetComponent<CollectFuse>().EnablePickUp();
        flashlight.layer = 8;
        piece.layer = 8;
        vp.SetActive(true);
        videoPlayer.Play();
        computerLight.gameObject.SetActive(true);
        blackoutSource = AudioManager.Instance.PlaySound(blackoutClip);
    }
    public void FuseBoxSecond()
    {
        MonologueManager.Instance.Notification();
        foreach (CeilingLamp1 lamp in lamps)
        {
            lamp.TurnAllOn();
        }
        foreach (GameObject obj in objectsToActivateAfterSecond)
        {
            obj.SetActive(true);
        }
        guard.SetActive(false);
        computerLight.gameObject.SetActive(false);
    }
}
