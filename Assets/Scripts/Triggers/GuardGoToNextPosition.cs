using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardGoToNextPosition : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<GuardScript>())
        {
            other.gameObject.GetComponent<GuardScript>().SetDestination(new Vector3(4.67000008f, 0.131520748f, 17.8299999f));
            Destroy(gameObject);
        }
    }
}
