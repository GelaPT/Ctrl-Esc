﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Save {
    static public void SaveData<T>(T data) {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + typeof(T) + ".data";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    static public T LoadData<T>() {
        string path = Application.persistentDataPath + "/" + typeof(T) + ".data";
        if (!File.Exists(path)) return default;

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Open);

        T data = (T)formatter.Deserialize(stream);
        stream.Close();

        return data;
    }
}