using UnityEngine;
using TMPro;
using System.Collections;


[System.Serializable]
public class Wire {
    public GameObject bone;
    public Vector3 startPos;
    public Outline outline;
    
    [HideInInspector] public bool attached = false;
    [HideInInspector] public EndWire attachedEndWire;
}

[System.Serializable]
public class EndWire {
    public GameObject sphere;
    
    [HideInInspector] public Outline outline;
    [HideInInspector] public bool occupied = false;
}

public class FuseboxTask : Task {
    [SerializeField] private Animator animator;
    public bool firstTime = true;
    public bool hasFuse = false;
    public bool fuseTime = false;
    [SerializeField] private Light pointLight;

    [SerializeField] private Wire[] wires;
    [SerializeField] private EndWire[] endWires;
    private int currentWire = -1;
    [SerializeField] private LayerMask startLayer;
    [SerializeField] private LayerMask endLayer;

    [SerializeField] private GameObject monologuePanel;
    [SerializeField] private TextMeshProUGUI subtitle;

    [SerializeField] private AfterFuseBox afb;

    private Task thisTask;

    private void Start() {
        foreach(Wire wire in wires) {
            wire.startPos = wire.bone.transform.localPosition;
        }

        foreach(EndWire endWire in endWires) {
            endWire.outline = endWire.sphere.GetComponent<Outline>();
        }
        thisTask = GetComponent<Task>();
    }

    private void Update() {
        if(!TaskManager.Instance.IsTask) {
            if(pointLight.enabled) pointLight.enabled = false;
            ChangeEndWireOutline(false);
            ChangeWireOutline(false);
            return;
        }

        if(TaskManager.Instance.currentTask != thisTask) {
            ChangeEndWireOutline(false);
            ChangeWireOutline(false);
            return;
        }

        if(firstTime) {
            firstTime = false;
            animator.SetBool("IsOpen", true);
        }

        if(currentWire == -1) {
            ChangeEndWireOutline(false);
            ChangeWireOutline(true);
        } else {
            ChangeEndWireOutline(true);
            ChangeWireOutline(false);
        }

        if(!pointLight.enabled) pointLight.enabled = true;

        if(taskCamera.transform.position != taskCameraTransform.position) {
            taskCamera.transform.position = Vector3.Lerp(taskCamera.transform.position, taskCameraTransform.position, Time.deltaTime * transitionSpeed);
        }

        if(taskCamera.transform.rotation != taskCameraTransform.rotation) {
            taskCamera.transform.rotation = Quaternion.Lerp(taskCamera.transform.rotation, taskCameraTransform.rotation, Time.deltaTime * transitionSpeed);
        }

        if(Input.GetKeyDown(KeyCode.Mouse0)) {
            if(currentWire != -1) {
                if(Physics.Raycast(taskCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 10f, endLayer)){
                    int endWire;
                    if((endWire = FindEndWire(hit.collider.gameObject)) != -1) {
                        if(((endWire < 3 && currentWire < 3) || (endWire > 2 && currentWire > 2)) && !endWires[endWire].occupied) {
                            wires[currentWire].bone.transform.position = hit.collider.gameObject.transform.position;
                            wires[currentWire].attached = true;
                            endWires[endWire].occupied = true;
                            wires[currentWire].attachedEndWire = endWires[endWire];
                            currentWire = -1;
                        }
                    }
                    return;
                }
            }
            if(Physics.Raycast(taskCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, 10f, startLayer)) {
                currentWire = FindWire(hitInfo.collider.gameObject);
                wires[currentWire].bone.transform.localPosition = wires[currentWire].startPos;
                if(wires[currentWire].attached) {
                    wires[currentWire].attached = false;
                    wires[currentWire].attachedEndWire.occupied = false;
                    wires[currentWire].attachedEndWire = null;
                    currentWire = -1;
                }
            }
        }
        foreach (Wire wire in wires)
        {
            if (wire.attachedEndWire == null)
                return;
        }

        if (wires[0].attachedEndWire.sphere == endWires[0].sphere &&
            (wires[1].attachedEndWire.sphere == endWires[1].sphere || wires[1].attachedEndWire.sphere == endWires[2].sphere) &&
            (wires[2].attachedEndWire.sphere == endWires[2].sphere || wires[2].attachedEndWire.sphere == endWires[1].sphere) &&
            wires[3].attachedEndWire.sphere == endWires[3].sphere &&
            wires[4].attachedEndWire.sphere == endWires[4].sphere) {
            if(!fuseTime) {
                afb.FuseBoxFirst();
                Destroy(monologuePanel);
                fuseTime = true;
                ResetFusebox();
            }
            else
            {
                GameManager.Instance.UpdateState(new GameManager.GameStates(GameManager.GameState.Running, GameManager.GameStage.SecondStage));
                TaskManager.Instance.CreateTasks();
                afb.FuseBoxSecond();
            }
            TaskManager.Instance.CompleteTask();
        }
    }

    private void ChangeWireOutline(bool value) {
        foreach(Wire wire in wires) {
            if(wire.outline) {
                if(wire.outline.enabled != value) wire.outline.enabled = value;
                if(wire.attached) wire.outline.enabled = false;
            }
        }
    }

    private void ChangeEndWireOutline(bool value) {
        foreach(EndWire wire in endWires) {
            if(wire.outline) {
                if(wire.outline.enabled != value) wire.outline.enabled = value;
                if(wire.occupied) wire.outline.enabled = false;
            }
        }
    }

    private int FindWire(GameObject bone) {
        for(int i = 0; i < wires.Length; i++) {
            if(wires[i].bone == bone) {
                return i;
            }
        }

        return -1;
    }

    private int FindEndWire(GameObject sphere) {
        for(int i = 0; i < endWires.Length; i++) {
            if(endWires[i].sphere == sphere) {
                return i;
            }
        }

        return -1;
    }
    private void ResetFusebox()
    {
        foreach(Wire wire in wires)
        {
            wire.attached = false;
            wire.attachedEndWire.occupied = false;
            wire.attachedEndWire = null;
            wire.bone.transform.localPosition = wire.startPos;
        }
    }
}