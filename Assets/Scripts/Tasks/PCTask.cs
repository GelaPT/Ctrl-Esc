using UnityEngine;

public class PCTask : Task {
    [SerializeField] private GameObject OS;
    [SerializeField] private Outline outline;
    [SerializeField] private Outline outline2;
    private Task thisTask;

    private void Awake() {
        thisTask = GetComponent<Task>();
    }

    private void Update() {
        if(!TaskManager.Instance) return;
        if(!TaskManager.Instance.IsTask) {
            if(OS.activeSelf) OS.SetActive(false);
            return;
        }

        if(TaskManager.Instance.currentTask == thisTask)
            if(!OS.activeSelf) {
                OS.SetActive(true);
            }
        
        if(taskCamera.transform.position != taskCameraTransform.position) {
            taskCamera.transform.position = Vector3.Lerp(taskCamera.transform.position, taskCameraTransform.position, Time.deltaTime * transitionSpeed);
        }

        if(taskCamera.transform.rotation != taskCameraTransform.rotation) {
            taskCamera.transform.rotation = Quaternion.Lerp(taskCamera.transform.rotation, taskCameraTransform.rotation, Time.deltaTime * transitionSpeed);
        }
    }
}
