using UnityEngine;

public class ProjectorTask : Task {
    //marks
    [SerializeField] private Projector marks;
    private Outline outline;

    //referencia dos sliders
    [SerializeField] private GameObject sliderX_OBJ;
    [SerializeField] private GameObject sliderY_OBJ;
    [SerializeField] private GameObject sliderScale_OBJ;
    private Outline xOutline;
    private Outline yOutline;
    private Outline sOutline;

    //valor de 0 a 360
    [SerializeField] private int sliderXValue;
    [SerializeField] private int sliderYValue;
    [SerializeField] private int sliderScaleValue;

    //referencia do Projector e o limite do quanto pode mover
    [SerializeField] private GameObject projection;
    [SerializeField] private float limits = 0.01f;

    //Task
    [SerializeField] private LayerMask wheelLayer;
    private int sliderXoffset;
    private int sliderYoffset;
    private int sliderSoffset;
    private static bool firstTime = true;
    [SerializeField] private GameObject hintPanel;

    private float lastSlide = 0.0f;
    private float slideDuration = 0.1f;

    private Task thisTask;

    public bool virustask = false;
    public CeilingLamp1[] lamps;

    void Start() {
        marks.nearClipPlane = projection.GetComponent<Projector>().nearClipPlane;
        marks.farClipPlane = projection.GetComponent<Projector>().farClipPlane;
        marks.fieldOfView = projection.GetComponent<Projector>().fieldOfView;

        outline = GetComponent<Outline>();
        thisTask = GetComponent<Task>();

        do {
            sliderXValue = Random.Range(0, 18) * 20;
        } while(sliderXValue == 180);

        do {
            sliderYValue = Random.Range(0, 18) * 20;
        } while(sliderYValue == 180);

        do {
            sliderScaleValue = Random.Range(0, 18) * 20;
        } while(sliderScaleValue == 180);

        xOutline = sliderX_OBJ.GetComponent<Outline>();
        yOutline = sliderY_OBJ.GetComponent<Outline>();
        sOutline = sliderScale_OBJ.GetComponent<Outline>();

        xOutline.enabled = false;
        yOutline.enabled = false;
        sOutline.enabled = false;

        UpdateProjector();
    }

    void Update() {
        //manager de task
        if(!TaskManager.Instance) return;
        if(!TaskManager.Instance.activeTasks.Contains(thisTask)) {
            if(outline.enabled) outline.enabled = false;
            projection.SetActive(false);
            marks.gameObject.SetActive(false);
            return;
        }

        projection.SetActive(true);
        marks.gameObject.SetActive(true);
        
        if(!TaskManager.Instance.IsTask) {
            if(!outline.enabled) outline.enabled = true;
            if(xOutline.enabled) xOutline.enabled = false;
            if(yOutline.enabled) yOutline.enabled = false;
            if(sOutline.enabled) sOutline.enabled = false;
            return;
        }

        if(TaskManager.Instance.currentTask != thisTask) {
            if(!outline.enabled) outline.enabled = true;
            if(xOutline.enabled) xOutline.enabled = false;
            if(yOutline.enabled) yOutline.enabled = false;
            if(sOutline.enabled) sOutline.enabled = false;
            return;
        }

        if(firstTime) {
            hintPanel.SetActive(true);
            firstTime = false;
        }

        if (taskCamera.transform.position != taskCameraTransform.position){
            taskCamera.transform.position = Vector3.Lerp(taskCamera.transform.position, taskCameraTransform.position, Time.deltaTime * transitionSpeed);
        }

        if (taskCamera.transform.rotation != taskCameraTransform.rotation){
            taskCamera.transform.rotation = Quaternion.Lerp(taskCamera.transform.rotation, taskCameraTransform.rotation, Time.deltaTime * transitionSpeed);
        }

        if(!xOutline.enabled) xOutline.enabled = true;
        if(!yOutline.enabled) yOutline.enabled = true;
        if(!sOutline.enabled) sOutline.enabled = true;

        // Detetar se est� a clicar na roda e se estiver adicionar o offset correto
        if(Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Mouse1)) {
            if(Physics.Raycast(taskCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 10f, wheelLayer)) {
                if(hit.collider.gameObject == sliderX_OBJ) {
                    if(Input.GetKeyDown(KeyCode.Mouse0)) {
                        sliderXoffset = -1;
                    } else {
                        sliderXoffset = 1;
                    }
                } else if(hit.collider.gameObject == sliderY_OBJ) {
                    if(Input.GetKeyDown(KeyCode.Mouse0)) {
                        sliderYoffset = 1;
                    } else {
                        sliderYoffset = -1;
                    }
                } else {
                    if(Input.GetKeyDown(KeyCode.Mouse0)) {
                        sliderSoffset = -1;
                    } else {
                        sliderSoffset = 1;
                    }
                }
            }
        }

        //Resetar o offset se n estiver a clicar nos bot�es
        if(!Input.GetKey(KeyCode.Mouse0) && !Input.GetKey(KeyCode.Mouse1)) {
            sliderXoffset = 0;
            sliderYoffset = 0;
            sliderSoffset = 0;
        }

        //Acrescentar o offset
        if(Time.time - lastSlide > slideDuration && (sliderXoffset != 0 || sliderYoffset != 0 || sliderSoffset != 0)) {
            sliderXValue = ChangeValue(sliderXValue, sliderXoffset * 10);
            sliderYValue = ChangeValue(sliderYValue, sliderYoffset * 10);
            sliderScaleValue = ChangeValue(sliderScaleValue, sliderSoffset * 20);
            lastSlide = Time.time;
        }

        UpdateProjector();

        //COMPLETAR A TASK
        if(sliderXValue == 180 && sliderYValue == 180 && sliderScaleValue == 180) {
            hintPanel.SetActive(false);
            TaskManager.Instance.CompleteTask();
            if (virustask)
            {
                foreach(CeilingLamp1 lamp in lamps)
                {
                    lamp.TurnAllOff();
                }
            }
        }
    }

    private void UpdateProjector() {
        projection.transform.localPosition = new Vector3(Mathf.Lerp(-limits, limits, sliderXValue / 360f), Mathf.Lerp(-limits, limits, sliderScaleValue / 360f), Mathf.Lerp(-limits, limits, sliderYValue / 360f));

        sliderX_OBJ.transform.localRotation = Quaternion.Euler(sliderXValue, 0, 0);
        sliderY_OBJ.transform.localRotation = Quaternion.Euler(sliderYValue, 0, 0);
        sliderScale_OBJ.transform.localRotation = Quaternion.Euler(0, sliderScaleValue, 0);
    }

    private int ChangeValue(int key, int value) {
        if (key >= 0 && key <= 360)
            key += value;
        if (key < 0)
            key = 360;
        else if (key > 360)
            key = 0;

        return key;
    }
}
