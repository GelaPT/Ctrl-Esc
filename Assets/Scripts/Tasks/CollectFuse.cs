using UnityEngine;

public class CollectFuse : MonoBehaviour
{
    public FuseboxTask fbt;

    void Start()
    {
        if (GameManager.Instance.CurrentGameState.gameStage != GameManager.GameStage.FirstStage)
        {
            Destroy(gameObject);
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current)
    {
        if (current.gameStage == GameManager.GameStage.SecondStage)
        {
            Destroy(gameObject);
        }
        if (current.gameStage == GameManager.GameStage.ThirdStage)
        {
            Destroy(gameObject);
        }
    }

    public void EnablePickUp()
    {
        GetComponent<Outline>().enabled = true;
    }
    public void PickUp()
    {
        TaskManager.Instance.AddTask(fbt);
        fbt.hasFuse = true;
        MonologueManager.Instance.Notification();
        Destroy(gameObject);
    }
}
