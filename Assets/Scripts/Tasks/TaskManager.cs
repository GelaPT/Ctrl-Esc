using System.Collections.Generic;
using UnityEngine;

public class Task : MonoBehaviour {
    public Camera taskCamera;
    public Transform taskCameraTransform;
    public float transitionSpeed;
    public Canvas taskCanvas;
    public OSManager osManager;
    public string taskID;
    [HideInInspector] public int uniqueID;
    public string room;
    [Range(1, 2)] public int floor;
    protected bool corrupted;
}

public class Room {
    public string room;
    public int floor;

    static public bool Has(Room room, List<Room> usedRooms) {
        foreach(Room rom in usedRooms) {
            if(rom.floor == room.floor && rom.room == room.room) {
                return true;
            }
        }

        return false;
    }
}

public class TaskManager : Singleton<TaskManager> {
    private bool isTask = false;
    public bool IsTask {
        get {
            return isTask;
        } set {
            isTask = value;
            ChangedIsTask();
        }
    }

    public List<Task> tasks = new List<Task>();

    public List<Task> activeTasks = new List<Task>();
    
    public Camera playerCamera;
    public Camera flashlightCamera;
    public Task currentTask;

    public bool hasPlayedProjector = false;
    private bool antivirusTask = true;

    public AudioClip sucessClip;
    public AudioClip failClip;

    private List<Room> usedRooms = new List<Room>();

    [SerializeField] private Door virusDoor;
    [SerializeField] private Outline virusOutline;
    [SerializeField] private Task virusTask;

    public void CompleteTask() {
        Outline[] outlines;
        if((outlines = currentTask.gameObject.GetComponentsInChildren<Outline>()).Length > 0) {
            foreach(Outline outline in outlines) {
                outline.enabled = false;
            }
        }
        activeTasks.Remove(currentTask);
        if(currentTask.taskID != "FuseboxTask" && activeTasks.Count > 1) playerCamera.GetComponentInParent<PlayerScript>().SaveGame();
        switch(GameManager.Instance.CurrentGameState.gameStage) {
            case GameManager.GameStage.FirstStage:
                if(currentTask.taskID != "FuseboxTask") {
                    if(activeTasks.Count < 1) {
                        foreach(Task task in tasks) {
                            if(task.GetType().Equals(typeof(FuseboxTask))) {
                                MonologueManager.Instance.SpeakMonologue(Random.Range(0, 2));
                                MonologueManager.Instance.Notification();
                                MonologueManager.Instance.ShowMonologue("namephil", "Hey, it's Phil, go check the fusebox in the file room, please!");
                                AddTask(task);
                            }
                        }
                    }
                }
                break;
            case GameManager.GameStage.SecondStage:
                if(currentTask == virusTask) {
                    GameManager.Instance.UpdateState(new GameManager.GameStates(GameManager.GameState.Running, GameManager.GameStage.ThirdStage));
                } else if(activeTasks.Count < 1) {
                    MonologueManager.Instance.Notification();
                    MonologueManager.Instance.ShowMonologue("namephil", "I am Phil. I request you to come to me in 2nd floor. Immediately.");
                    virusDoor.Unlock();
                    virusDoor.ForceOpen();
                    virusOutline.enabled = true;
                }
                break;
            default:
                break;
        }
        IsTask = false;
        AudioManager.Instance.PlaySoundAtLocation(sucessClip, transform.position, 2);
    }

    public void PlayTask(Task task) {
        if(activeTasks.Contains(task)) {

            if(!hasPlayedProjector) {
                if(task.GetType() == typeof(ProjectorTask)) {
                    hasPlayedProjector = true;
                }
            }

            currentTask = task;
            Outline[] outlines;
            if((outlines = task.gameObject.GetComponentsInChildren<Outline>()).Length > 0) {
                foreach(Outline outline in outlines) {
                    outline.enabled = false;
                }
            }
            IsTask = true;
        }
    }

    public void LeaveTask() {
        Outline[] outlines;
        if((outlines = currentTask.gameObject.GetComponentsInChildren<Outline>()).Length > 0) {
            foreach(Outline outline in outlines) {
                outline.enabled = true;
            }
        }

        currentTask.gameObject.SetActive(false);
        currentTask.gameObject.SetActive(true);

        IsTask = false;
        currentTask = null;
    }

    protected override void Awake() {
        base.Awake();
        foreach(Task task in tasks) {
            if(task.GetType().Equals(typeof(PCTask))) {
                PCTask pctask = task as PCTask;
                pctask.osManager.antivirusTask = antivirusTask;
                antivirusTask = !antivirusTask;

            }
        }
    }

    private void Start() {
        if(GameManager.Instance.saveData.taskID.Count > 0) {
            int avtask = 0, wctask = 0, projtask = 0;
            foreach(string str in GameManager.Instance.saveData.taskID) {
                if(str == "avtask") {
                    avtask++;
                } else if(str == "wctask") {
                    wctask++;
                } else if(str == "projtask") {
                    projtask++;
                }
            }

            if(avtask != 0 || wctask != 0) activeTasks.AddRange(ChooseRandomPCTasks(avtask, wctask));
            if(projtask != 0) activeTasks.AddRange(ChooseRandomProjectorTasks(projtask));
        }

        GameManager.Instance.onGameStateChanged.AddListener(OnGameStateChanged);
        
        if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage) {
            virusDoor.Unlock();
        }

        RefreshOutlines();
    }

    public void CreateTasks() {
        if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage) {
            activeTasks.AddRange(ChooseRandomPCTasks(1, 2));
            activeTasks.AddRange(ChooseRandomProjectorTasks(2));
        } else {
            activeTasks.AddRange(ChooseRandomPCTasks(2, 2));
            activeTasks.AddRange(ChooseRandomProjectorTasks(2));
        }

        RefreshOutlines();
    }

    private List<Task> ChooseRandomPCTasks(int avamount, int wcamount) {
        int i = 0;
        List<Task> returnTasks = new List<Task>();

        while(i < avamount) {
            foreach(Task task in tasks) {
                Room room = new Room();
                room.room = task.room;
                room.floor = task.floor;
                if(Room.Has(room, usedRooms)) continue;
                if(task.GetType().Equals(typeof(PCTask))) {
                    if(!returnTasks.Contains(task) && !activeTasks.Contains(task)) {
                        PCTask pctask = task as PCTask;
                        if(pctask.osManager.antivirusTask) {
                            if(Random.Range(0, 60) == 0 && i < avamount) {
                                returnTasks.Add(task);
                                usedRooms.Add(room);
                                i++;
                            }
                        }
                    }
                }
            }
        }

        int j = 0;
        while(j < wcamount) {
            foreach(Task task in tasks) {
                Room room = new Room();
                room.room = task.room;
                room.floor = task.floor;
                if(Room.Has(room, usedRooms)) continue;
                if(task.GetType().Equals(typeof(PCTask))) {
                    if(!returnTasks.Contains(task) && !activeTasks.Contains(task)) {
                        PCTask pctask = task as PCTask;
                        if(!pctask.osManager.antivirusTask) {
                            if(Random.Range(0, 60) == 0 && j < wcamount) {
                                returnTasks.Add(task);
                                usedRooms.Add(room);
                                j++;
                            }
                        }
                    }
                }
            }
        }

        return returnTasks;
    }

    private List<Task> ChooseRandomProjectorTasks(int amount) {
        int i = 0;
        List<Task> returnTasks = new List<Task>();
        while(i < amount) {
            foreach(Task task in tasks) {
                Room room = new Room {
                    room = task.room,
                    floor = task.floor
                };
                if(Room.Has(room, usedRooms)) continue;
                if(task.GetType().Equals(typeof(ProjectorTask))) {
                    if(!returnTasks.Contains(task) && !activeTasks.Contains(task)) {
                        if(Random.Range(0, 60) == 0 && i < amount) {
                            returnTasks.Add(task);
                            usedRooms.Add(room);
                            i++;
                        }
                    }
                }
            }
        }

        return returnTasks;
    }

    private void RefreshOutlines() {
        foreach(Task task in activeTasks) {
            if(currentTask)
                if(task == currentTask) 
                    continue;
            Outline[] outlines;
            if((outlines = task.gameObject.GetComponentsInChildren<Outline>()).Length > 0)
                foreach(Outline outline in outlines) {
                    outline.enabled = true;
                }
        }
    }

    public void AddTask(Task task) {
        activeTasks.Add(task);
        Outline[] outlines;
        if((outlines = task.gameObject.GetComponentsInChildren<Outline>()).Length > 0) {
            foreach(Outline outline in outlines) {
                outline.enabled = true;
            }
        }
    }

    public void ChangedIsTask() {
        playerCamera.enabled = !isTask;
        flashlightCamera.enabled = !isTask;
        playerCamera.gameObject.SetActive(!isTask);
        currentTask.taskCamera.enabled = isTask;
        if(isTask) {
            currentTask.taskCamera.transform.position = playerCamera.transform.position;
            currentTask.taskCamera.transform.rotation = playerCamera.transform.rotation;
            Cursor.lockState = CursorLockMode.Confined;
        } else {
            Cursor.lockState = CursorLockMode.Locked;
        }
        RefreshOutlines();
    }

    public void OnGameStateChanged(GameManager.GameStates previous, GameManager.GameStates current) {
        if(current.gameStage == GameManager.GameStage.ThirdStage) {
            virusDoor.Unlock();
        }
    }
}
