using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour {
    private void Start() {
        AudioManager.Instance.gameObject.SetActive(false);
        GameManager.Instance.UpdateState(new GameManager.GameStates(GameManager.GameState.Running, GameManager.GameStage.LastCut));
    }
    public void LeaveGame() {
        AudioManager.Instance.ResetAudio();
        AudioManager.Instance.gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.Confined;
        SceneManager.LoadScene(0);
    }
}
