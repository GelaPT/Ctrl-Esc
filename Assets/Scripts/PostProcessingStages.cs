using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessingStages : MonoBehaviour
{
    public PostProcessProfile[] profiles;
    public PostProcessVolume ppv;

    private void Start()
    {
        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.FirstStage) {
            ppv.profile = profiles[0];
        } else if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage) {
            ppv.profile = profiles[1];
        } else if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage) {
            ppv.profile = profiles[2];
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current) {
        if (current.gameStage == GameManager.GameStage.FirstStage) {
            ppv.profile = profiles[0];
        } else if (current.gameStage == GameManager.GameStage.SecondStage) {
            ppv.profile = profiles[1];
        } else if (current.gameStage == GameManager.GameStage.ThirdStage) {
            ppv.profile = profiles[2];
        }
    }
}
