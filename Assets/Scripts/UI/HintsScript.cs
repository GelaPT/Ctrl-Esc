using TMPro;
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class HintKey {
    public KeyCode key;
    [HideInInspector] public bool clicked;
}

public class HintsScript : MonoBehaviour {
    [SerializeField] private List<HintKey> keys;
    [SerializeField] private PlayerScript player;
    private bool active = false;
    [SerializeField] private bool tutorial;
    [SerializeField] private GameObject hintPanel;
    [SerializeField] private TextMeshProUGUI hintText;
    [SerializeField] private HintsScript next;
    [TextArea(10, 30)] public string hint;

    private void Start() {
        if(GameManager.Instance.saveData.taskID.Count != 0 || !tutorial) Destroy(gameObject);
    }

    private void OnTriggerStay(Collider other) {
        if(!GetComponent<HintsScript>().enabled) return;
        if(other.gameObject.tag == "Player")
        {
            hintPanel.gameObject.SetActive(true);
            hintText.text = hint;
            active = true;
        }
    }

    private void Update() {
        if(!active) return;

        foreach(HintKey key in keys) {
            if(!key.clicked) {
                if(Input.GetKeyDown(key.key)) {
                    key.clicked = true;
                }
                return;
            }
        }

        Deactivate();
    }

    private void Deactivate() {
        active = false;
        if(next) next.enabled = true;
        hintPanel.gameObject.SetActive(false);
        hintText.text = "";
        Destroy(gameObject);
    }
}