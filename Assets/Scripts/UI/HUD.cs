using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField] private PlayerScript playerscript;

    [SerializeField] private Slider staminaBar;

    private Animator animator;

    private bool showStaminaBar;
    private float staminaBarTimer = 0;
    private float timeToHideStaminaBar = 2;

    void Start()
    {
        animator = GetComponent<Animator>();
        staminaBar.maxValue = playerscript.maxStamina;
        staminaBar.value = playerscript.stamina;
    }

    void Update()
    {
        staminaBar.maxValue = playerscript.maxStamina;
        staminaBar.value = playerscript.stamina;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            staminaBarTimer = 0;
            showStaminaBar = true;
        }
        else
        {
            if (staminaBarTimer > timeToHideStaminaBar)
                showStaminaBar = false;
            else
                staminaBarTimer += Time.deltaTime;
        }
        animator.SetBool("staminaBarOn", showStaminaBar);
    }
}
