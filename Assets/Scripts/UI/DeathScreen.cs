using UnityEngine;

public class DeathScreen : MonoBehaviour {
    [SerializeField] private GameObject player;
    [SerializeField] private MouseLook mouseLook;
    [SerializeField] private GameObject deadManequin;
    [SerializeField] private GameObject subtitlePanel;
    public void RetryButton() {
        player.GetComponent<CharacterController>().enabled = false;
        player.transform.position = new Vector3(48.5f, 6.5f, -10f);
        player.GetComponent<CharacterController>().enabled = true;
        player.GetComponent<PlayerScript>().canMove = true;
        mouseLook.canLook = true;
        deadManequin.transform.position = new Vector3(-5.86f, 4.94f, 1.25f);
        Cursor.lockState = CursorLockMode.Locked;
        gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void QuitButton() {
        GameManager.Instance.UpdateState(new GameManager.GameStates(GameManager.GameState.PreGame, GameManager.Instance.CurrentGameState.gameStage));
        Time.timeScale = 1;
        GameManager.LoadScene(1);
    }
}
