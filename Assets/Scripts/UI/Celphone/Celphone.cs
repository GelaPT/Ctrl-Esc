using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using TMPro;

public class Celphone : MonoBehaviour
{
    //ligar e desligar
    private bool isOn = false;
    private Animator animator;
    [SerializeField] MouseLook mouselook;
    [SerializeField] private GameObject phoneTaskPrefab;
    [SerializeField] private Transform phoneTaskParent;
    [SerializeField] private GameObject backbutton;
    private List<GameObject> phoneTasks = new List<GameObject>();
    [SerializeField] private FuseboxTask fusebox;
    [SerializeField] private GameObject flashlight;
    [SerializeField] private Task virusProjector;

    //horas no cel
    [SerializeField] TextMeshProUGUI clock;

    private void Start()
    {
        GameManager.Instance.onGameStateChanged.AddListener(GameStateChanged);
        animator = GetComponent<Animator>();
    }
    private void Update()
    {
        if(isOn)
            GetTime();
    }

    private void GetTime()
    {
        DateTime now = DateTime.Now;
        clock.text = now.ToString("HH:mm");
    }

    //fechar celular pelo botao de resume
    public void CloseCelphone()
    {
        backbutton.SetActive(false);
        GameManager.Instance.UnPause();
    }

    public void GameStateChanged(GameManager.GameStates previous, GameManager.GameStates current) {
        if(current.gameState == GameManager.GameState.Paused) {
            Cursor.lockState = CursorLockMode.Confined;
            isOn = true;
            animator.SetBool("IsOn", isOn);
            foreach(GameObject gameObj in phoneTasks) {
                Destroy(gameObj);
            }
            phoneTasks.Clear();

            if(!GuardDialogueScript.talkedToGuard && GameManager.Instance.saveData.taskID.Count == 0) {
                GameObject phoneTaskObject = Instantiate(phoneTaskPrefab, phoneTaskParent);
                phoneTasks.Add(phoneTaskObject);
                PhoneTask phoneTask = phoneTaskObject.GetComponent<PhoneTask>();

                phoneTask.taskName.text = "Talk the to Guard";
                phoneTask.taskRoom.text = "Security";
                phoneTask.taskFloor.text = "1";
                return;
            }

            if(!fusebox.firstTime && !fusebox.hasFuse) {
                GameObject phoneTaskObject = Instantiate(phoneTaskPrefab, phoneTaskParent);
                phoneTasks.Add(phoneTaskObject);
                PhoneTask phoneTask = phoneTaskObject.GetComponent<PhoneTask>();

                if(flashlight.activeSelf) {
                    phoneTask.taskName.text = "Pick up Piece";
                    phoneTask.taskRoom.text = "Storage B";
                    phoneTask.taskFloor.text = "1st";
                    return;
                }
                phoneTask.taskName.text = "Pick up Flashlight";
                phoneTask.taskRoom.text = "File Room";
                phoneTask.taskFloor.text = "1";
                return;
            }

            if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage && TaskManager.Instance.activeTasks.Count < 1) {
                GameObject phoneTaskObject = Instantiate(phoneTaskPrefab, phoneTaskParent);
                phoneTasks.Add(phoneTaskObject);
                PhoneTask phoneTask = phoneTaskObject.GetComponent<PhoneTask>();

                phoneTask.taskName.text = "Find Phil";
                phoneTask.taskRoom.text = "I";
                phoneTask.taskFloor.text = "2";
                return;
            }

            if(TaskManager.Instance.activeTasks.Contains(virusProjector)) {
                GameObject phoneTaskObject = Instantiate(phoneTaskPrefab, phoneTaskParent);
                phoneTasks.Add(phoneTaskObject);
                PhoneTask phoneTask = phoneTaskObject.GetComponent<PhoneTask>();

                phoneTask.taskName.text = "Fix Projector";
                phoneTask.taskRoom.text = "I";
                phoneTask.taskFloor.text = "2";

                phoneTaskObject = Instantiate(phoneTaskPrefab, phoneTaskParent);
                phoneTasks.Add(phoneTaskObject);
                phoneTask = phoneTaskObject.GetComponent<PhoneTask>();

                phoneTask.taskName.text = "Leave";
                phoneTask.taskRoom.text = "Exit";
                phoneTask.taskFloor.text = "1";
                return;
            }

            if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage) {
                GameObject phoneTaskObject = Instantiate(phoneTaskPrefab, phoneTaskParent);
                phoneTasks.Add(phoneTaskObject);
                PhoneTask phoneTask = phoneTaskObject.GetComponent<PhoneTask>();

                phoneTask.taskName.text = "Rush to";
                phoneTask.taskRoom.text = "Power Room";
                phoneTask.taskFloor.text = "1";
                return;
            }

            foreach(Task task in TaskManager.Instance.activeTasks) {
                GameObject phoneTaskObject = Instantiate(phoneTaskPrefab, phoneTaskParent);
                phoneTasks.Add(phoneTaskObject);
                PhoneTask phoneTask = phoneTaskObject.GetComponent<PhoneTask>();

                string taskRoom;
                char c;

                switch(task.taskID) {
                    case "PCTask":
                        PCTask pcTask = task as PCTask;
                        taskRoom = task.room;
                        c = taskRoom[0];
                        c = char.ToUpper(c);
                        taskRoom = taskRoom.Substring(1);
                        taskRoom = c + taskRoom;
                        phoneTask.taskName.text = pcTask.osManager.antivirusTask ? "Run Antivirus" : "Run WCleaner";
                        phoneTask.taskRoom.text = GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.FirstStage ? taskRoom : "?";
                        phoneTask.taskFloor.text = task.floor == 1 ? "1st" : "2nd";
                        break;
                    case "ProjectorTask":
                        taskRoom = task.room;
                        c = taskRoom[0];
                        c = char.ToUpper(c);
                        taskRoom = taskRoom.Substring(1);
                        taskRoom = c + taskRoom;
                        phoneTask.taskName.text = "Fix Projector";
                        phoneTask.taskRoom.text = GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.FirstStage ? taskRoom : "?";
                        phoneTask.taskFloor.text = task.floor == 1 ? "1st" : "2nd";
                        break;
                    case "FuseboxTask":
                        phoneTask.taskName.text = "Fix Fusebox";
                        phoneTask.taskRoom.text = "File Room";
                        phoneTask.taskFloor.text = "1st";
                        break;
                }
            }
    } else if(current.gameState == GameManager.GameState.Running && previous.gameState == GameManager.GameState.Paused) {
            isOn = false;
            animator.SetBool("IsOn", isOn);
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
