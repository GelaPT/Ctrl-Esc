using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[System.Serializable]
public struct MMButton {
    public Button button;
    public Image image;
    public TextMeshProUGUI text;
    public int id;
    public GameObject subMenu;
    public GameObject subMenuFirst;
}

public class MainMenuButtonManager : MonoBehaviour, IEventSystemHandler {
    public List<MMButton> buttons = new List<MMButton>();
    [SerializeField] private Button continueButton;
    private int selectedButton = 0;
    private int selectedMenu = -1;

    [SerializeField] private Sprite unselectedSprite;
    [SerializeField] private Sprite selectedSprite;

    [SerializeField] private Color selectedColor;
    [SerializeField] private Color unselectedColor;

    [SerializeField] private AudioClip clickClip;
    [SerializeField] private AudioClip menuAmbient;
    [SerializeField] private AudioClip pcOn;
    private AudioSource menuAmbientSource;

    private void Start() {
        if(GameManager.Instance.saveData != null) continueButton.interactable = true;
        else continueButton.interactable = false;

        foreach(MMButton button in buttons) {
            button.button.onClick.AddListener(() => ButtonClicked(button.id));
        }
        Invoke("PCLoop", pcOn.length);
    }

    private void PCLoop() {
        menuAmbientSource = AudioManager.Instance.PlaySound(menuAmbient, true);
    }

    private void Update() {
        if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
            if(!EventSystem.current.currentSelectedGameObject) {
                EventSystem.current.SetSelectedGameObject(buttons[selectedButton].button.gameObject);
            }
        }

        if(selectedMenu == -1) return;

        if(Input.GetKeyDown(KeyCode.Joystick1Button1)) {
            Selectable selectable = EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>();
            if(selectable.navigation.selectOnLeft) {
                EventSystem.current.SetSelectedGameObject(selectable.navigation.selectOnLeft.gameObject);
            }
        }
    }

    public void StartNewGame() {
        AudioManager.Instance.StopSound(menuAmbientSource);
        GameManager.Instance.StartNewGame();
    }

    public void ContinueGame() {
        AudioManager.Instance.StopSound(menuAmbientSource);
        GameManager.Instance.ContinueGame();
    }

    private void UpdateSelectedButton() {
        foreach(MMButton button in buttons) {
            button.image.sprite = selectedMenu == button.id ? selectedSprite : unselectedSprite;
            button.image.color = selectedMenu == button.id ? selectedColor : unselectedColor;
            button.text.color = selectedMenu == button.id ? selectedColor : unselectedColor;
            button.subMenu.SetActive(selectedMenu == button.id ? true : false);
            Navigation nav = new Navigation();
            nav.mode = Navigation.Mode.Explicit;
            nav.selectOnDown = button.button.navigation.selectOnDown;
            nav.selectOnUp = button.button.navigation.selectOnUp;
            nav.selectOnRight = button.subMenu.activeSelf ? button.subMenuFirst.GetComponent<Selectable>() : null;
            button.button.navigation = nav;
        }
    }

    private void ButtonClicked(int buttonNumber) {
        if(selectedMenu == buttonNumber) selectedMenu = -1;
        else selectedMenu = buttonNumber;
        selectedButton = buttonNumber;
        AudioManager.Instance.PlaySound(clickClip);
        UpdateSelectedButton();
    }

    public void DeselectAll() {
        selectedMenu = -1;
        UpdateSelectedButton();
    }

    public void PlayClickSound()
    {
        AudioManager.Instance.PlaySound(clickClip);
    }
}