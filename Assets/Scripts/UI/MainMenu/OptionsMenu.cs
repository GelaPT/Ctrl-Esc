using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

public class OptionsMenu : MonoBehaviour
{
    //Options Loading
    [SerializeField] private Slider sensSlider;
    [SerializeField] private Slider volSlider;
    [SerializeField] private Slider brightSlider;

    //volume
    [SerializeField] private AudioClip clip;
    private AudioSource currentSource;

    //resolution
    public int width = 1920;
    public int height = 1080;
    [SerializeField] private TextMeshProUGUI currText;
    [SerializeField] private TextMeshProUGUI otherText;

    //fullscreen
    [SerializeField] private TextMeshProUGUI fsText;

    //camera motion
    [SerializeField] private TextMeshProUGUI cmText;

    //brightness
    [SerializeField] PostProcessProfile[] ppp;

    private void Start() {
        OptionsData optionsData = OptionsManager.Instance.optionsData;
        sensSlider.value = optionsData.sensitivity;
        volSlider.SetValueWithoutNotify(optionsData.volume);
        brightSlider.value = optionsData.brightness;

        ChangeRes();
        ChangeRes();
        SetFullScreen();
        SetFullScreen();
        SetCameraMotion();
        SetCameraMotion();

        Cursor.lockState = CursorLockMode.Confined;

        gameObject.SetActive(false);
    }

    public void SetSensitivity(float value) {
        OptionsManager.Instance.Sensitivity = (int)value;
    }

    //volume
    public void SetMasterVolume(float value)
    {
        OptionsManager.Instance.Volume = (int)value;
        if(currentSource) AudioManager.Instance.StopSound(currentSource);
        AudioManager.Instance.ChangeVolume();
        PlayClickAudio();
    }

    public void PlayClickAudio() {
        currentSource = AudioManager.Instance.PlaySound(clip, false);
    }

    public void ChangeRes() {
        OptionsManager.Instance.FullHD = !OptionsManager.Instance.FullHD;

        width = OptionsManager.Instance.FullHD ? 1920 : 1280;
        height = OptionsManager.Instance.FullHD ? 1080 : 720;

        currText.text = (OptionsManager.Instance.FullHD ? 1920 : 1280) + "x" + (OptionsManager.Instance.FullHD ? 1080 : 720);
        otherText.text = (OptionsManager.Instance.FullHD ? 1080 : 1920) + "x" + (OptionsManager.Instance.FullHD ? 720 : 1080);
        Screen.SetResolution(width, height, OptionsManager.Instance.Fullscreen);
    }

    public void SetFullScreen()
    {
        OptionsManager.Instance.Fullscreen = !OptionsManager.Instance.Fullscreen;

        Screen.SetResolution(width, height, OptionsManager.Instance.Fullscreen);
        if (OptionsManager.Instance.Fullscreen)
        {
            fsText.text = "On";
        }
        else
        {
            fsText.text = "Off";
        }
    }

    //camera motion
    public void SetCameraMotion()
    {
        OptionsManager.Instance.CameraMotion = !OptionsManager.Instance.CameraMotion;
        
        if (OptionsManager.Instance.CameraMotion)
        {
            cmText.text = "On";
        }
        else
        {
            cmText.text = "Off";
        }
    }

    //brightness
    public void SetBrightness(float value)
    {
        OptionsManager.Instance.Brightness = (int)value;
        foreach(PostProcessProfile pp in ppp)
        {
            float x = pp.GetSetting<ColorGrading>().gamma.value.x;
            float y = pp.GetSetting<ColorGrading>().gamma.value.y;
            float z = pp.GetSetting<ColorGrading>().gamma.value.z;
            pp.GetSetting<ColorGrading>().gamma.value = new Vector4(x, y, z, (value / 5) - 1);
        }

    }

    public void Quit() {
        Application.Quit();
    }

    public void PhoneQuit() {
        GameManager.Instance.UpdateState(new GameManager.GameStates(GameManager.GameState.PreGame, GameManager.Instance.CurrentGameState.gameStage));
        Time.timeScale = 1;
        GameManager.LoadScene(1);
    }
}