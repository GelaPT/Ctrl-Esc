using UnityEngine;

public class TitleScreen : MonoBehaviour {
    public GameObject mainScreen;
    public AudioClip pcTurnOn;
    public Animator mainCameraAnimator;

    private void Start() {
        AudioManager.Instance.ResetAudio();
        GameManager.Instance.saveData = SaveManager.LoadGameSave();
    }

    void Update() {
        if(Input.anyKeyDown) {
            AudioManager.Instance.PlaySound(pcTurnOn);
            mainScreen.SetActive(true);
            Destroy(gameObject);
        }
    }
}
