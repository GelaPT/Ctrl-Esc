using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private AudioClip doorOpen;
    [SerializeField] private AudioClip doorClose;
    [SerializeField] private AudioClip doorLocked;

    public bool isOpen;
    public bool isLocked;

    public bool PhilDoor;

    private Animator _animator;
    private void Awake()
    {
        _animator = GetComponent<Animator>();

        _animator.SetBool("isOpen", isOpen);

        _animator.SetBool("isLocked", isLocked);
    }

    public void Start()
    {
        if ((GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage ||
            GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage ||
            GameManager.Instance.saveData.guard == 1)
            && PhilDoor)
        {
            ForceClose();
            Lock();
        }
    }

    public void Open()
    {
        if (!isLocked)
        {
            isOpen = !isOpen;
            _animator.SetBool("isOpen", isOpen);
        }
        else if (isLocked && isOpen == false)
        {
            _animator.Play("Base Layer.door_locked", -1, 0f);
            AudioManager.Instance.PlaySoundAtLocation(doorLocked, transform.position, 2f, false);
        }
    }

    public void Lock()
    {
        isLocked = true;
        _animator.SetBool("isLocked", isLocked);
        _animator.SetBool("isOpen", isOpen);
    }

    public void Unlock()
    {
        isLocked = false;
        _animator.SetBool("isLocked", isLocked);
        _animator.SetBool("isOpen", isOpen);
    }

    public void ForceClose()
    {
        isOpen = false;
        _animator.SetBool("isOpen", isOpen);
        _animator.SetBool("isLocked", isLocked);
    }

    public void ForceOpen()
    {
        isOpen = true;
        _animator.SetBool("isOpen", isOpen);
        _animator.SetBool("isLocked", isLocked);
    }

    public void PlayOpenSound()
    {
        AudioManager.Instance.PlaySoundAtLocation(doorOpen, transform.position, 2f, false);
    }

    public void PlayCloseSound()
    {
        AudioManager.Instance.PlaySoundAtLocation(doorClose, transform.position, 2f, false);
    }
}
