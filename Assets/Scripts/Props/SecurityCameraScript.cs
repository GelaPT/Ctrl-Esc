using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityCameraScript : MonoBehaviour {
    [SerializeField] private GameObject camera_OBJ;

    [SerializeField] private float turningSpeed;

    [SerializeField] private Transform[] lookAtPoints;
    [SerializeField] private Light spotlight;

    public Vector3 offset;
    [SerializeField] private Transform target;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private LayerMask wallMask;
    [SerializeField] private Transform player;

    [SerializeField] private DeadMannequinScript deadman;
    private float timer;

    private bool enemy;

    void Start() {
        target.transform.localPosition = lookAtPoints[0].transform.localPosition;
        target.transform.localRotation = lookAtPoints[0].transform.localRotation;

        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage)
        {
            enemy = true;
            spotlight.gameObject.SetActive(true);
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }

    void LateUpdate() {
        //if stage == primeiro n�o fazer nada || segundo n�o avisar robos || avisar robos
        if(enemy)
            if(player) {
                camera_OBJ.transform.LookAt(player);
                camera_OBJ.transform.Rotate(offset);
                return;
            }

        timer += Time.deltaTime * turningSpeed;
        float alpha = (Mathf.Cos(timer) + 1) / 2;
        target.transform.localPosition = Vector3.Lerp(lookAtPoints[0].transform.localPosition, lookAtPoints[1].transform.localPosition, alpha);
        camera_OBJ.transform.LookAt(target);
        camera_OBJ.transform.Rotate(offset);
    }

    private void FixedUpdate() {
        if (!enemy) return;
        if(player) {
            if (!Physics.Raycast(transform.position, player.position - transform.position, out _, Vector3.Distance(player.position, transform.position), wallMask)) {
                return;
            }
            player = null;
            return;
        }

        if(Physics.Raycast(transform.position, target.position - transform.position, out RaycastHit hit, 999f, playerMask)) {
            player = hit.transform;
            if (deadman != null)
            {
                deadman.foundPlayer = true;
                deadman.Player = player.gameObject;
            }
            if(!Physics.Raycast(transform.position, player.position - transform.position, out _, Vector3.Distance(player.position, transform.position), wallMask)) {
                return;
            }
        }
        player = null;
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current)
    {
        if (current.gameStage == GameManager.GameStage.ThirdStage)
        {
            spotlight.gameObject.SetActive(true);
            enemy = true;
        }
    }
}
