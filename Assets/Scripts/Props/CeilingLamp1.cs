using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CeilingLamp1 : MonoBehaviour
{
    public bool playsound;
    [SerializeField] private AudioClip turnOn;
    [SerializeField] private AudioClip loop;
    [SerializeField] private bool loopFlick = true;
    private AudioSource loopAudioSource;
    private bool shouldLoopAudio = false;

    private Animator _animator;

    [SerializeField] private bool alwaysOn = false;
    [SerializeField] private bool isOn;

    //Vari�vel para caso o player saie e volte a entrar na luz, para ela n�o apagar
    private bool isUnderLight = false;

    //delay pra apagar a luz
    [SerializeField] private float delayMax;
    [SerializeField] private float delayMin;
    private float delay;

    [SerializeField] private BoxCollider box;

    public bool viruslamp = false;


    void Start()
    {
        delay = Random.Range(delayMin, delayMax); //definir delay random
        _animator = GetComponent<Animator>(); //pegar o animator

        if (alwaysOn) isOn = true; //ligar caso seja uma luz sempre ligada

        //atualizar a variavel no animator pelas fun��es TurnOn e Off
        if (isOn) TurnOn();
        else Invoke("TurnOff", delay);
        box.enabled = true;
        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage && !viruslamp)
        {
            TurnAllOff();
            TurnAllOn();
        }
        else if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage)
        {
            TurnAllOff();
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }

    private void Update()
    {
        _animator.SetBool("isOn", isOn);
    }

    public void TurnOn()
    {
        if (!isOn)
        {
            isOn = true;
            _animator.SetBool("isOn", isOn);
            if (playsound)
            {
                AudioManager.Instance.PlaySoundAtLocation(turnOn, transform.position, 3);
                if (loopFlick)
                {
                    Invoke("Loop", turnOn.length);
                    shouldLoopAudio = true;
                }
            } 
        }
    }

    public void TurnOff()
    {
        if(isOn && !isUnderLight && !alwaysOn) //apenas apagar se ele estiver fora da collision box e se n�o for do tipo alwaysOn
        {
            isOn = false;
            _animator.SetBool("isOn", isOn);
            if (playsound)
            {
                shouldLoopAudio = false;
                if (loopAudioSource) AudioManager.Instance.StopSound(loopAudioSource);
            }
        }

    }

    private void Loop()
    {
        if (shouldLoopAudio)
            loopAudioSource = AudioManager.Instance.PlaySoundAtLocation(loop, transform.position, 3, true);
    }

    //checar colis�o se est� debaixo da luz, na collision box
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            TurnOn();
            isUnderLight = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) {
            isUnderLight = false;
            Invoke("TurnOff", delay);
        }
    }

    public void TurnAllOff()
    {
        if (!viruslamp)
        {
            Invoke("TurnOff", 0f);
            box.enabled = false;
            alwaysOn = false;
            isOn = false; 
        }
    }

    public void TurnAllOn()
    {
        if (!viruslamp)
        {
            delayMin = 10;
            delayMax = 15;
            box.enabled = true;
            alwaysOn = false;
            isOn = false; 
        }
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current)
    {
        if (current.gameStage == GameManager.GameStage.SecondStage)
        {
            TurnAllOn();
        }
        else if (current.gameStage == GameManager.GameStage.ThirdStage)
        {
            TurnAllOff();
        }
    }

    public void ForceTurnOn()
    {
        isOn = true;
        _animator.SetBool("isOn", isOn);
        if (playsound)
        {
            AudioManager.Instance.PlaySoundAtLocation(turnOn, transform.position, 3);
            if (loopFlick)
            {
                Invoke("Loop", turnOn.length);
                shouldLoopAudio = true;
            }
        }
    }

    public void ForceTurnOff()
    {
        isOn = false;
        _animator.SetBool("isOn", isOn);
        if (playsound)
        {
            shouldLoopAudio = false;
            if (loopAudioSource) AudioManager.Instance.StopSound(loopAudioSource);
        }
    }
}
