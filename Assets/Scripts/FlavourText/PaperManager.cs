using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperManager : Singleton<PaperManager>
{
    public MouseLook mouselook;
    public GameObject textPanel;
    public FlavourTextDisplayScript flavourtextdisplayscript;
}
