using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FlavourTextDisplayScript : MonoBehaviour
{
    [SerializeField] private AudioClip paperClip;
    public string[] text;
    private int pageIndex = -1;
    [SerializeField] private TextMeshProUGUI displayText;
    [SerializeField] private Button nxtBt;
    [SerializeField] private Button prvBt;
    void Start()
    {
        DisplayText();
    }

    public void SetupText(string[] texts)
    {
        nxtBt.interactable = false;
        prvBt.interactable = false;

        text = texts;
        pageIndex = 0;

        if(text.Length > 1)
        {
            nxtBt.interactable = true;
        }
        DisplayText();
    }

    public void DisplayText() {
        displayText.text = text[pageIndex];
    }

    public void NextPage()
    {
        PaperManager.Instance.flavourtextdisplayscript.SetupText(text);
        pageIndex++;
        if (pageIndex == text.Length - 1)
            nxtBt.interactable = false;
        prvBt.interactable = true;
        DisplayText();
    }

    public void PreviousPage()
    {
        PaperManager.Instance.flavourtextdisplayscript.SetupText(text);
        pageIndex--;
        if (pageIndex == 0)
            prvBt.interactable = false;
        nxtBt.interactable = true;
        DisplayText();
    }

    public void CloseDisplay()
    {
        text = null;
        pageIndex = -1;

        nxtBt.interactable = false;
        prvBt.interactable = false;

        Cursor.lockState = CursorLockMode.Locked;

        PaperManager.Instance.mouselook.canLook = true;

        gameObject.SetActive(false);
    }
}
