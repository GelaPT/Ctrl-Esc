using UnityEngine;

public class FlavourTextScript : MonoBehaviour
{
    [SerializeField] private AudioClip paperClip;
    [TextArea(10, 30)]
    public string[] text;

    public void ShowTextOnScreen()
    {
        AudioManager.Instance.PlaySound(paperClip);
        PaperManager.Instance.mouselook.canLook = false;
        PaperManager.Instance.textPanel.SetActive(true);
        PaperManager.Instance.flavourtextdisplayscript.SetupText(text);
        Cursor.lockState = CursorLockMode.Confined;
    }
}
