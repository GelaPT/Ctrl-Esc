using UnityEngine;
using UnityEngine.Video;

public class Cutscene : MonoBehaviour {
    [SerializeField] private VideoPlayer vp;
    [SerializeField] private GameObject pausedMenu;
    private bool paused;

    private void Start() {
        vp.loopPointReached += EndReached;
        vp.SetDirectAudioVolume(0, OptionsManager.Instance.Volume/10f);
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            TogglePause();
        }
    }

    public void TogglePause() {
        if(paused) {
            Cursor.lockState = CursorLockMode.Locked;
            vp.Play();
        } else {
            Cursor.lockState = CursorLockMode.Confined;
            vp.Pause();
        }
        paused = !paused;
        pausedMenu.SetActive(paused);
    }

    private void EndCut() {
        GameManager.LoadScene(3);
        GameManager.Instance.UpdateState(new GameManager.GameStates(GameManager.GameState.Running, GameManager.GameStage.FirstStage));
    }

    public void SkipCutscene() {
        EndCut();
    }

    private void EndReached(VideoPlayer vp) {
        EndCut();
    }
}
