using UnityEngine;

public class OptionsManager : Singleton<OptionsManager> {
    private OptionsData optData;
    public OptionsData optionsData => optData;
    public int Sensitivity {
        get {
            return optionsData.sensitivity;
        } set {
            optionsData.sensitivity = value;
            OnVariableChange();
        }
    }
    public int Volume {
        get {
            return optionsData.volume;
        } set {
            optionsData.volume = value;
            OnVariableChange();
        }
    }
    public int Brightness {
        get {
            return optionsData.brightness;
        } set {
            optionsData.brightness = value;
            OnVariableChange();
        }
    }
    public bool FullHD {
        get {
            return optionsData.fullHD;
        } set {
            optionsData.fullHD = value;
            OnVariableChange();
        }
    }
    public bool Fullscreen {
        get {
            return optionsData.fullscreen;
        } set {
            optionsData.fullscreen = value;
            OnVariableChange();
        }
    }
    public bool CameraMotion {
        get {
            return optionsData.cameraMotion;
        } set {
            optionsData.cameraMotion = value;
            OnVariableChange();
        }
    }

    protected override void Awake() {
        base.Awake();
        optData = SaveManager.LoadOptions();
    }

    public void OnVariableChange() {
        SaveManager.SaveOptions(optData);
    }
}
