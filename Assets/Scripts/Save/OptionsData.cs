[System.Serializable]
public class OptionsData {
    public int sensitivity;
    public int volume;
    public int brightness;
    public bool fullHD;
    public bool fullscreen;
    public bool cameraMotion;

    public OptionsData() {
        sensitivity = 3;
        volume = 7;
        brightness = 5;
        fullHD = true;
        fullscreen = true;
        cameraMotion = true;
    }

    public OptionsData(int sens, int vol, int bright, bool fullhd, bool fullScreen, bool cameraMot) {
        sensitivity = sens;
        volume = vol;
        brightness = bright;
        fullHD = fullhd;
        fullscreen = fullScreen;
        cameraMotion = cameraMot;
    }
}