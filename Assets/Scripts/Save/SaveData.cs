using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaitVector3 {
    public float x, y, z;
    public WaitVector3() {
        x = 0;
        y = 0;
        z = 0;
    }

    public WaitVector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public WaitVector3(Vector3 vec) {
        x = vec.x;
        y = vec.y;
        z = vec.z;
    }

    public override string ToString() {
        return "X: " + x + " Y: " + y + " Z: " + z;
    }
}

[System.Serializable]
public class WaitQuaternion {
    public float x, y, z, w;
    public WaitQuaternion() {
        x = Quaternion.identity.x;
        y = Quaternion.identity.y;
        z = Quaternion.identity.z;
        w = Quaternion.identity.w;
    }

    public WaitQuaternion(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public WaitQuaternion(Quaternion quat) {
        x = quat.x;
        y = quat.y;
        z = quat.z;
        w = quat.w;
    }

    public override string ToString() {
        return "X: " + x + " Y: " + y + " Z: " + z + " W: " + w;
    }
}

[System.Serializable]
public class SaveData {
    public int stage;
    public WaitVector3 vec;
    public WaitQuaternion quat;
    public bool lantern;
    public int guard;
    public List<string> taskID;

    public SaveData() {
        stage = 1;
        vec = new WaitVector3(3, 3, 3);
        quat = new WaitQuaternion(Quaternion.identity);
        lantern = false;
        guard = 0;
        taskID = new List<string>();
    }

    public SaveData(int _stage, Vector3 pos, Quaternion rot, bool _lantern, int _guard, List<string> _taskID) {
        stage = _stage;
        vec = new WaitVector3(pos);
        quat = new WaitQuaternion(rot);
        lantern = _lantern;
        guard = _guard;
        taskID = _taskID;
    }
}