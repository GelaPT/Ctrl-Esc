public class SaveManager {
    static public void SaveOptions(OptionsData optionsData) {
        Save.SaveData(optionsData);
    }

    static public OptionsData LoadOptions() {
        OptionsData data;
        if((data = Save.LoadData<OptionsData>()) == default) {
            SaveOptions(new OptionsData());
            return LoadOptions();
        }

        return data;
    }

    static public void SaveGame(SaveData saveData) {
        Save.SaveData(saveData);
    }

    static public SaveData LoadGameSave() {
        SaveData data;
        if((data = Save.LoadData<SaveData>()) == default) {
            return null;
        }

        return data;
    }
}