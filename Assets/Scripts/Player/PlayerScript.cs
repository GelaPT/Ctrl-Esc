using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterController))]
public class PlayerScript: MonoBehaviour {
    public List<AmbientSound> ambientSounds = new List<AmbientSound>();

    public float speed = 7.5f;
    public float sprintSpeed = 4f;

    public float maxStamina = 10;
    [HideInInspector] public float stamina;
    private float staminaIncrease = 3;
    private float staminaDecrease = 1;
    [SerializeField] private float timeToRecover = 1f;
    private float recoverTimer = 0;

    public float gravity = -9.81f;
    public Vector3 move;
    [HideInInspector] public bool canMove = true;

    [SerializeField] private Transform groundCheck;
    [SerializeField] private float groundDistance = 0.4f;
    [SerializeField] private LayerMask groundMask;
    private bool isGrounded = false;

    [SerializeField] private GameObject flashlight;

    private CharacterController characterController;
    private Vector3 velocity;

    [SerializeField] private Camera fpsCamera;

    //Sounds
    [SerializeField] private AudioClip walkingClip;
    [SerializeField] private AudioClip runningClip;
    private AudioSource walkingSource;
    private AudioSource runningSource;

    [HideInInspector] public HeadBob headBob;
    [HideInInspector] public HeadBob flashBob;

    [SerializeField] private GameObject deathPanel;
    [SerializeField] private GameObject emprego;
    [SerializeField] private GameObject subtitlePanel;

    [SerializeField] private GuardScript guard;

    void Start() {
        if(GameManager.Instance.saveData.taskID.Count != 0 || GameManager.Instance.CurrentGameState.gameStage != GameManager.GameStage.FirstStage) {
            Destroy(emprego);
            flashlight.SetActive(GameManager.Instance.saveData.lantern);
        }
        subtitlePanel.SetActive(false);
        headBob = fpsCamera.GetComponent<HeadBob>();
        flashBob = flashlight.GetComponent<HeadBob>();

        characterController = GetComponent<CharacterController>();
        stamina = maxStamina;

        characterController.enabled = false;
        transform.position = new Vector3(GameManager.Instance.saveData.vec.x, GameManager.Instance.saveData.vec.y, GameManager.Instance.saveData.vec.z);
        transform.rotation = new Quaternion(GameManager.Instance.saveData.quat.x, GameManager.Instance.saveData.quat.y, GameManager.Instance.saveData.quat.z, GameManager.Instance.saveData.quat.w);
        characterController.enabled = true;

        AudioManager.Instance.ambientSounds = ambientSounds;
        AudioManager.Instance.playerCam = fpsCamera.gameObject;
        AudioManager.Instance.PlayAmbientSounds();

        GameManager.Instance.onGameStateChanged.AddListener(OnGameStatesChanged);
    }

    void Update() {
        if(GameManager.Instance.CurrentGameState.gameState == GameManager.GameState.Paused) return;
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        
        if(isGrounded && velocity.y < 0) {
            velocity.y = 0.0f;
        } else {
            velocity.y += gravity * Time.deltaTime;
        }
        if(canMove)
            characterController.Move(velocity * Time.deltaTime);

        if(!canMove) {
            if(walkingSource) AudioManager.Instance.StopSound(walkingSource);
            if(runningSource) AudioManager.Instance.StopSound(runningSource);
            return;
        }
        if(TaskManager.Instance.IsTask) {
            if(walkingSource) AudioManager.Instance.StopSound(walkingSource);
            if(runningSource) AudioManager.Instance.StopSound(runningSource);
            return;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        move = transform.right * x + transform.forward * z;
        if (move.magnitude > 1) {
            move.Normalize();
        }

        if(move.magnitude < 0.1f) {
            if(walkingSource) AudioManager.Instance.StopSound(walkingSource);
            if(runningSource) AudioManager.Instance.StopSound(runningSource);
        }

        if (z > 0 && Mathf.Abs(x) < 0.1f && stamina > 0 && Input.GetKey(KeyCode.LeftShift) && canMove) {
            if(walkingSource) AudioManager.Instance.StopSound(walkingSource);
            if(!runningSource) runningSource = AudioManager.Instance.PlaySound(runningClip, true);
            headBob.running = true;
            flashBob.running = true;

            stamina = Mathf.Clamp(stamina - (staminaDecrease * Time.deltaTime), 0.0f, maxStamina);
            recoverTimer = 0;

            //mudar fov
            fpsCamera.fieldOfView = 91f;
            characterController.Move(move * sprintSpeed * Time.deltaTime);
        } else {
            headBob.running = false;
            flashBob.running = false;
            if(runningSource) AudioManager.Instance.StopSound(runningSource);
            if(!walkingSource) walkingSource = AudioManager.Instance.PlaySound(walkingClip, true);

            if (stamina < maxStamina) {
                if (recoverTimer >= timeToRecover)
                    stamina = Mathf.Clamp(stamina + (staminaIncrease * Time.deltaTime), 0.0f, maxStamina);
                else
                    recoverTimer += Time.deltaTime;
            }

            //resetar fov
            fpsCamera.fieldOfView = 90f;
            characterController.Move(move * speed * Time.deltaTime);
        }
    }

    public void SaveGame() {
        List<string> tasks = new List<string>();
        tasks.Add("jogueiojogomanoooo");
        foreach(Task task in TaskManager.Instance.activeTasks) {
            if(task.taskID == "ProjectorTask") {
                tasks.Add("projtask");
            } else if(task.taskID == "PCTask") {
                if((task as PCTask).osManager.antivirusTask) {
                    tasks.Add("avtask");
                } else {
                    tasks.Add("wctask");
                }
            }
        }

        switch(GameManager.Instance.CurrentGameState.gameStage) {
            case GameManager.GameStage.FirstStage:
                SaveManager.SaveGame(new SaveData(1, transform.position, transform.rotation, flashlight.activeSelf, guard.state, tasks));
                break;
            case GameManager.GameStage.SecondStage:
                SaveManager.SaveGame(new SaveData(2, transform.position, transform.rotation, flashlight.activeSelf, guard.state, tasks));
                break;
        }
    }

    public void Stun()
    {
        canMove = false;
        Invoke("RecoverFromStun", 4f);
    }
    private void RecoverFromStun()
    {
        canMove = true;
    }

    private void OnGameStatesChanged(GameManager.GameStates previous, GameManager.GameStates current) {
        if(current.gameState == GameManager.GameState.Paused) {
            if(walkingSource) AudioManager.Instance.StopSound(walkingSource);
            if(runningSource) AudioManager.Instance.StopSound(runningSource);
        }
    }

    public void Died()
    {
        deathPanel.SetActive(true);
    }
}