using UnityEngine;

public class HeadBob : MonoBehaviour
{
    public bool walkingSimulation = false;
    public bool running = false;
    [SerializeField] private float runningMultiply = 1.2f;

    public float walkingBobbingSpeedX;
    public float walkingBobbingSpeedY;
    public float bobbingAmountX = 0.05f;
    public float bobbingAmountY = 0.05f;

    [SerializeField] private PlayerScript controller;
    private float defaultPosX = 0;
    private float defaultPosY = 0;
    private float timerX = 0;
    private float timerY = 0;

    void Start() {
        defaultPosX = transform.localPosition.x;
        defaultPosY = transform.localPosition.y;
    }

    void Update() {
        if(!OptionsManager.Instance.CameraMotion) return;
        if(!walkingSimulation || controller == null) {
            timerX += Time.deltaTime * walkingBobbingSpeedX;
            timerY += Time.deltaTime * walkingBobbingSpeedY;
            transform.localPosition = new Vector3(defaultPosX + Mathf.Cos(timerX) * bobbingAmountX, defaultPosY + Mathf.Sin(timerY) * bobbingAmountY, transform.localPosition.z);
            return;
        }

        if (controller.move.magnitude > 0.1f) {
            timerX += Time.deltaTime * walkingBobbingSpeedX * (running ? runningMultiply : 1);
            timerY += Time.deltaTime * walkingBobbingSpeedY * (running ? runningMultiply : 1);
            transform.localPosition = new Vector3(defaultPosX + Mathf.Sin(timerX) * bobbingAmountX * controller.move.magnitude, defaultPosY + Mathf.Sin(timerY) * bobbingAmountY * controller.move.magnitude, transform.localPosition.z);
        } else {
            timerX = 0;
            timerY = 0;
            transform.localPosition = new Vector3(Mathf.Lerp(transform.localPosition.x, defaultPosX, Time.deltaTime * walkingBobbingSpeedX), Mathf.Lerp(transform.localPosition.y, defaultPosY, Time.deltaTime * walkingBobbingSpeedY), transform.localPosition.z);
        }
    }
}