﻿using UnityEngine;

public class CameraInteractScript : MonoBehaviour {
    public bool PerformRayCasting<T>(out T value) {
        RaycastHit hit;
        Ray forwardRay = new Ray(transform.position, transform.forward);

        if(!InteractManager.Instance) {
            value = default(T);
            return false;
        }
        if(Physics.Raycast(forwardRay, out hit, InteractManager.Instance.sightDistance, InteractManager.Instance.layerMask))
            if(hit.collider.gameObject.TryGetComponent(out value))
                return true;

        value = default(T);
        return false;
    }
}
