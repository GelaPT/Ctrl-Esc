using UnityEngine;

public class MouseLook : MonoBehaviour {
    public Transform playerBody;

    private float xRotation = 0.0f;

    public bool canLook = true;

    void Start() {
        Invoke("LockMouse", 0.5f);
    }
    private void LockMouse()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update() {
        if(!TaskManager.Instance) return;
        if(TaskManager.Instance.IsTask) return;
        if (!canLook) return;
        float mouseX = Input.GetAxisRaw("Mouse X") * OptionsManager.Instance.Sensitivity * 50 * Time.deltaTime;
        float mouseY = Input.GetAxisRaw("Mouse Y") * OptionsManager.Instance.Sensitivity * 50 * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -89f, 89f);

        if(xRotation < -360) {
            xRotation += 360;
        } else if(xRotation > 360) {
            xRotation -= 360;
        }

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }
}