using UnityEngine;

public class ColliderInteractScript : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.TryGetComponent(out Door door))
            InteractManager.Instance.collidingDoor = door;
    }

    private void OnTriggerExit(Collider other) {
        if(InteractManager.Instance.collidingDoor != null)
            if(other.gameObject.Equals(InteractManager.Instance.collidingDoor.gameObject))
                InteractManager.Instance.collidingDoor = null;
    }
}
