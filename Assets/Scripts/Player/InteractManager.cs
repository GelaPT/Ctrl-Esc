using UnityEngine;

public class InteractManager : Singleton<InteractManager> {
    [SerializeField] private CameraInteractScript cameraInteractScript;
    [SerializeField] private ColliderInteractScript colliderInteractScript;

    [SerializeField] private GameObject crosshair;

    public float sightDistance = 2f;
    public LayerMask layerMask;

    public Door collidingDoor = null;
    public Door door = null;
    public GuardDialogueScript guard = null;
    public FlavourTextScript text = null;
    public Task task = null;
    public FlashlightPickup flashlight = null;
    public CollectFuse fuse = null;
    public VirusDialogueScript virus = null;
    public VirusDialogue2Script virus2 = null;

    private void FixedUpdate() {
        cameraInteractScript.PerformRayCasting(out door);
        cameraInteractScript.PerformRayCasting(out guard);
        cameraInteractScript.PerformRayCasting(out text);
        cameraInteractScript.PerformRayCasting(out task);
        cameraInteractScript.PerformRayCasting(out flashlight);
        cameraInteractScript.PerformRayCasting(out fuse);
        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage) {
            cameraInteractScript.PerformRayCasting(out virus);
            cameraInteractScript.PerformRayCasting(out virus2);
        }

        if (door || guard || text || task || collidingDoor || flashlight || fuse || virus || virus2) {
            crosshair.SetActive(true);
        } else {
            crosshair.SetActive(false);
        }
    }

    private void Update() {
        if(GameManager.Instance.CurrentGameState.gameState == GameManager.GameState.Paused) return;

        if(TaskManager.Instance)
            if(TaskManager.Instance.currentTask)
                if(TaskManager.Instance.currentTask.osManager)
                    if(TaskManager.Instance.currentTask.osManager.isWriting) return;
        

        if(Input.GetKeyDown(KeyCode.E)) {
            if(collidingDoor) {
                collidingDoor.Open();
            } else if(door) {
                door.Open();
            }
            
            if(guard) {
                guard.NextDialogue();
            }

            if (virus) {
                virus.NextDialogue();
            }
            if (virus2)
            {
                virus2.NextDialogue();
            }
            if (text) {
                text.ShowTextOnScreen();
            }
            if (flashlight)
            {
                flashlight.PickUp();
            }
            if (fuse)
            {
                fuse.PickUp();
            }

            if(TaskManager.Instance.IsTask) {
                TaskManager.Instance.LeaveTask();
            } else if(task) {
                TaskManager.Instance.PlayTask(task);
            }
        }
    }
}
