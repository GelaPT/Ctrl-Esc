using UnityEditor;

public class Shortcuts {
    [MenuItem("Tools/Toggle Inspector Lock %l")]
    public static void ToggleInspectorLock() {
        ActiveEditorTracker.sharedTracker.isLocked = !ActiveEditorTracker.sharedTracker.isLocked;
        ActiveEditorTracker.sharedTracker.ForceRebuild();
    }
}
