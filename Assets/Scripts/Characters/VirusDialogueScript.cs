using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusDialogueScript : DialogueScriptManager
{
    public Color viruscolor;
    public Task projectorTask;
    private bool _enabled = true;

    private int counter = -1; //numero atual do dialogo, onde vai escolher a fala, anima�ao e target point pra andar

    private float timer = 10f;

    private bool firstTime = true;
    private bool enableTimer = false;

    public PlayerScript player;
    public Outline outline;

    public AudioClip ambient;
    private AudioSource ambientSource;
    public AudioClip[] audios;

    public GameObject projection;

    public GameObject BadEndingTrigger;

    public GameObject pointlight;

    public CeilingLamp1[] lamps;

    void Start()
    {
        NextDialogue();
    }

    public void Update()
    {
        if (_enabled && enableTimer)
        {
            if (timer > 0)
            {
                timer -= 1 * Time.deltaTime;
            }
            else if (timer < 0)
            {
                subtitlePanel.SetActive(false);
            }
        }
    }

    public void NextDialogue() {
        if (firstTime) {
            firstTime = false;
            return;
        }
        subtitlePanel.SetActive(true);
        if (counter < lines.Length - 1)
        {
            counter += 1;
            switch (lines[counter])
            {
                case "start":
                    enableTimer = true;
                    player.canMove = false;
                    ambientSource = AudioManager.Instance.PlaySound(ambient, true);
                    Debug.Log(ambientSource.clip.name);
                    projection.SetActive(true);
                    foreach(CeilingLamp1 lamp in lamps)
                    {
                        lamp.ForceTurnOff();
                    }
                    pointlight.SetActive(true);
                    NextDialogue();
                    break;
                case "player":
                    subtitle.color = playercolor;
                    personName.text = "Me";
                    NextDialogue();
                    break;
                case "virus":
                    subtitle.color = viruscolor;
                    personName.text = "Virus";
                    NextDialogue();
                    break;
                case "01":
                    AudioManager.Instance.PlaySound(audios[0]);
                    Invoke("FearNot",audios[0].length);
                    NextDialogue();
                    break;
                case "02":
                    AudioManager.Instance.PlaySound(audios[1]);
                    NextDialogue();
                    break;
                case "03":
                    AudioManager.Instance.PlaySound(audios[2]);
                    NextDialogue();
                    break;
                case "04":
                    AudioManager.Instance.PlaySound(audios[3]);
                    NextDialogue();
                    break;
                case "05":
                    AudioManager.Instance.PlaySound(audios[4]);
                    NextDialogue();
                    break;
                case "06":
                    AudioManager.Instance.PlaySound(audios[5]);
                    NextDialogue();
                    break;
                case "07":
                    AudioManager.Instance.PlaySound(audios[6]);
                    NextDialogue();
                    break;
                case "08":
                    AudioManager.Instance.PlaySound(audios[7]);
                    NextDialogue();
                    break;
                case "end":
                    _enabled = false;
                    subtitle.text = "";
                    subtitlePanel.gameObject.SetActive(false);
                    counter = -1;
                    player.canMove = true;
                    AudioManager.Instance.StopSound(ambientSource);
                    BadEndingTrigger.SetActive(true);
                    foreach (CeilingLamp1 lamp in lamps)
                    {
                        lamp.ForceTurnOn();
                    }
                    pointlight.SetActive(false);
                    Invoke("EnableTask", 0.15f);
                    break;
                default:
                    StopCoroutine("PlayText");
                    StartCoroutine("PlayText", lines[counter]);
                    timer = 10f;
                    break;
            }
        }
    }

    IEnumerator PlayText(string line)
    {
        subtitle.text = "";
        foreach (char c in line)
        {
            subtitle.text += c;
            yield return new WaitForSeconds(0.05f);
        }
    }

    private void EnableTask()
    {
        projection.SetActive(false);
        projectorTask.enabled = true;
        outline.enabled = true;
        TaskManager.Instance.AddTask(projectorTask);
        MonologueManager.Instance.Notification();
        Destroy(gameObject);
    }

    private void FearNot()
    {
        AudioManager.Instance.PlaySound(audios[4]);
    }
}
