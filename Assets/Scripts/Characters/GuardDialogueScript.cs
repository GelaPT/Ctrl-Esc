using System.Collections;
using UnityEngine;

public class GuardDialogueScript : DialogueScriptManager {
    [HideInInspector] static public bool talkedToGuard = false;
    [SerializeField] private GuardScript gs;
    [SerializeField] private GuardHeadTurn ght;
    [SerializeField] LayerMask layer;
    [SerializeField] AudioClip[] voices;
    private bool _enabled = true;

    private int counter = -1; //numero atual do dialogo, onde vai escolher a fala, anima�ao e target point pra andar

    private float timer = 10f;
    [SerializeField] private Outline outline;

    void Start()
    {
        if(GameManager.Instance.CurrentGameState.gameStage != GameManager.GameStage.FirstStage) {
            Destroy(gameObject);
            return;
        }
        if(GameManager.Instance.saveData.taskID.Count != 0) {
            counter = lines.Length - 2;
        }
        outline = GetComponent<Outline>();
        outline.enabled = true;
        NextDialogue();
    }

    public void Update() {
        if(GameManager.Instance.CurrentGameState.gameStage != GameManager.GameStage.FirstStage) {
            Destroy(gameObject);
            return;
        }
        if (_enabled)
        {
            if(timer > 0)
            {
                timer -= 1 * Time.deltaTime;
            }
            else if(timer < 0)
            {
                Instance.subtitlePanel.SetActive(false);
            }
        }
    }

    public void NextDialogue() {
        if(gameObject.activeSelf && GameManager.Instance.CurrentGameState.gameStage != GameManager.GameStage.FirstStage) {
            gameObject.SetActive(false);
            return;
        }
        Instance.subtitlePanel.SetActive(true);
        if (counter < lines.Length - 1)
        {
            counter += 1;
            switch (lines[counter])
            {
                case "player":
                    Instance.subtitle.color = Instance.playercolor;
                    Instance.personName.text = "Me";
                    NextDialogue();
                    break;
                case "nameguard":
                    Instance.subtitle.color = Instance.othercolor;
                    Instance.personName.text = "Security";
                    NextDialogue();
                    break;
                case "namephil":
                    Instance.subtitle.color = Instance.othercolor;
                    Instance.personName.text = "Phil";
                    NextDialogue();
                    break;
                case "wave":
                    gs.animator.Play("Base Layer.StartWave");
                    NextDialogue();
                    break;
                case "stopwave":
                    gs.animator.Play("Base Layer.EndWave");
                    NextDialogue();
                    break;
                case "talk1":
                    gs.animator.Play("Base Layer.Talk01");
                    NextDialogue();
                    break;
                case "talk2":
                    gs.animator.Play("Base Layer.Talk02");
                    NextDialogue();
                    break;
                case "handin":
                    gs.animator.Play("Base Layer.HandIn");
                    NextDialogue();
                    break;
                case "walk":
                    outline.enabled = false;
                    gs.SetDestination(new Vector3(4f, 0.13f, 20.75f)); 
                    gs.currSpeed = gs.fullspeed;
                    NextDialogue();
                    break;
                case "end":
                    _enabled = false;
                    talkedToGuard = true;
                    if(GameManager.Instance.saveData.taskID.Count == 0) TaskManager.Instance.CreateTasks();
                    MonologueManager.Instance.Notification();
                    Instance.subtitle.text = "";
                    Instance.subtitlePanel.gameObject.SetActive(false);
                    counter = -1;
                    gameObject.GetComponent<GuardDialogueScript>().enabled = false;
                    gameObject.layer = layer;
                    gs.state = 1;
                    break;
                case "speak":
                    int i = Random.Range(0, voices.Length - 1);
                    AudioManager.Instance.PlaySound(voices[i]);
                    NextDialogue();
                    break;
                default:
                    StopCoroutine("PlayText");
                    StartCoroutine("PlayText", lines[counter]);
                    timer = 10f;
                    break;
            }
        }
    }

    IEnumerator PlayText(string line)
    {
        Instance.subtitle.text = "";
        foreach (char c in line)
        {
            Instance.subtitle.text += c;
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void StopHeadTurn()
    {
        ght.canlook = false;
    }
    public void ResumeHeadTurn()
    {
        ght.canlook = true;
    }
}