using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusDialogue2Script : DialogueScriptManager
{
    public Color viruscolor;
    private bool _enabled = true;

    private int counter = -1; //numero atual do dialogo, onde vai escolher a fala, anima�ao e target point pra andar

    private float timer = 10f;

    private bool enableTimer = false;

    public PlayerScript player;

    public BadEndingTrigger badending;

    public GameObject badendingcamera;

    public AudioClip[] audios;

    public AudioClip ambient;
    private AudioSource ambientSource;

    public AudioClip badclip;

    void Start()
    {
        NextDialogue();
    }

    public void Update()
    {
        if (_enabled && enableTimer)
        {
            if (timer > 0)
            {
                timer -= 1 * Time.deltaTime;
            }
            else if (timer < 0)
            {
                Instance.subtitlePanel.SetActive(false);
            }
            
        }
    }

    public void NextDialogue() {
        DialogueScriptManager.Instance.subtitlePanel.SetActive(true);
        if (counter < lines.Length - 1)
        {
            counter += 1;
            switch (lines[counter])
            {
                case "start":
                    enableTimer = true;
                    player.canMove = false;
                    ambientSource = AudioManager.Instance.PlaySound(ambient, true);
                    NextDialogue();
                    break;
                case "player":
                    Instance.subtitle.color = Instance.playercolor;
                    Instance.personName.text = "Me";
                    NextDialogue();
                    break;
                case "virus":
                    Instance.subtitle.color = viruscolor;
                    Instance.personName.text = "Virus";
                    NextDialogue();
                    break;
                case "01":
                    AudioManager.Instance.PlaySound(audios[0]);
                    NextDialogue();
                    break;
                case "02":
                    AudioManager.Instance.PlaySound(audios[1]);
                    NextDialogue();
                    break;
                case "03":
                    AudioManager.Instance.PlaySound(audios[2]);
                    NextDialogue();
                    break;
                case "04":
                    AudioManager.Instance.PlaySound(audios[3]);
                    NextDialogue();
                    break;
                case "end":
                    AudioManager.Instance.PlaySound(badclip);
                    _enabled = false;
                    Instance.subtitle.text = "";
                    Instance.subtitlePanel.gameObject.SetActive(false);
                    counter = -1;
                    badending.i = 1;
                    badending.UpdateAnimator();
                    badendingcamera.SetActive(true);
                    break;
                default:
                    StopCoroutine("PlayText");
                    StartCoroutine("PlayText", lines[counter]);
                    timer = 10f;
                    break;
            }
        }
    }

    IEnumerator PlayText(string line)
    {
        Instance.subtitle.text = "";
        foreach (char c in line)
        {
            Instance.subtitle.text += c;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
