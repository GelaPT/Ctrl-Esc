using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GuardScript : MonoBehaviour
{
    public NavMeshAgent agent;
    public Animator animator;

    [Range(0, 5)] public float fullspeed, lessSpeed;
    public float lerpSpeed;
    public float currSpeed;

    public int state;

    public Outline outline;

    void Start()
    {
        state = GameManager.Instance.saveData.guard;
        if(state == 0)
        {
            transform.position = new Vector3(-2.43000007f, 0.131520748f, 19.2630005f);
            outline.enabled = true;
        }else if(state == 1)
        {
            transform.position = new Vector3(5.4289999f, 0.671999991f, 15.184f);
            outline.enabled = false;
        }

        agent = GetComponent<NavMeshAgent>();

        currSpeed = 0;

        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage)
        {
            Destroy(gameObject);
        }
        else if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage)
        {
            Destroy(gameObject);
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }

    public void SetDestination(Vector3 targetpoint)
    {
        agent.SetDestination(targetpoint);
    }

    void Update()
    {
        animator.SetFloat("Speed", agent.desiredVelocity.magnitude);
        agent.speed = Mathf.Lerp(agent.speed, currSpeed, Time.deltaTime * lerpSpeed);
    }

    public void FullSpeed()
    {
        currSpeed = fullspeed;
    }

    public void LessSpeed()
    {
        currSpeed = lessSpeed;
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current)
    {
        if (current.gameStage == GameManager.GameStage.SecondStage)
        {
            Destroy(gameObject);
        }
        if (current.gameStage == GameManager.GameStage.ThirdStage)
        {
            Destroy(gameObject);
        }
    }
}
