using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RoombaScript : MonoBehaviour
{
    //Como funciona:
    //-Procura um lugar ao redor do transform.position
    //-Anda ate esse lugar
    //-Quando o remainingDistance for 0, parar de andar
    //-Acha uma nova posi��o e invocar a fun�ao de retomar a speed depois de x segundos
    //Caso seja um inimigo:
    //-Quando chega no destino, faz o Glitch
    //-Durante a opera�ao, se ele acha o player, deixa de andar por ai e s� segue o player, soltando sparkle a cada 3 segundos
    private AudioSource audioSource;

    [SerializeField] private LayerMask wallMask;
    [SerializeField] private LayerMask playerMask;
    private NavMeshAgent agent;

    public bool isEnemy = false;
    [SerializeField] private GameObject[] path;
    private int pathIndex = 0;
    
    private Vector3 target; //destino do ai destination

    public float Speed = 1f; //velocidade pra alterar aqui pra ser mais easy
    private float _speed; //velocidade original do agent

    public float restoreSpeedDelay = 1; //delay quando chega no destino e espera pra come�ar a andar dnovo

    private float timer = 0; //timer pra caso fique stuck
    private Vector3 currentPosition; //variavel de suporte no Glitch

    private bool foundPlayer;
    private GameObject player;
    [SerializeField] private AudioClip shock;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        agent.speed = Speed;
        _speed = agent.speed;

        SetDestination();

        audioSource = GetComponent<AudioSource>();
        audioSource.volume = OptionsManager.Instance.Volume / 10f;

        if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.FirstStage)
        {
            gameObject.SetActive(false);
        }
        else if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage)
        {
            if(isEnemy)
                gameObject.SetActive(false);
        }
        else if (GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage)
        {
            if(isEnemy)
                gameObject.SetActive(true);
            else
                gameObject.SetActive(false);
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }

    private void Update()
    {
        audioSource.volume = OptionsManager.Instance.Volume / 10f;
        if (!foundPlayer) //enquanto n acha o player, ta no modo de andar aleatorio. O roomba normal nunca sai desse estado
        {
            if (agent.remainingDistance == 0) //se chegou ao destino
            {
                if (isEnemy) //se for um inimigo, ao chegar no destino, fazer o glitch e seta o novo destination. Senao, deixa passar e s� seta o novo destination
                {
                    currentPosition = transform.position;
                    float c = 0;
                    while (c < 0.2)
                    {
                        Invoke("Glitch", c);
                        c += 0.02f;
                    }
                }
                SetDestination();
            }
        }
        else //se ele achar o player, passa a sempre segui-lo
        {
            target = player.transform.position;
            agent.SetDestination(target);
            timer -= Time.deltaTime;

            if (timer <= 0) //perder o player
            {
                foundPlayer = false;
                player = null;
                SetDestination();
            }
        }
    }
    private void FixedUpdate()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, transform.forward, out hit, 10f, wallMask) && isEnemy)
        {
            if (Physics.SphereCast(transform.position, 2f, transform.forward, out hit, 10f, playerMask))
            {
                timer = 10;
                if (!foundPlayer)
                {
                    foundPlayer = true;
                    player = hit.collider.gameObject;
                    SetDestination();
                    Invoke("ResetSpeed", 0f);
                }
            }
        }
    }
    private void SetDestination()
    {
        pathIndex = Random.Range(0, path.Length-1);

        target = path[pathIndex].transform.position;
        agent.SetDestination(target);
        if (foundPlayer)
        {
            Invoke("ResetSpeed", Random.Range(0.7f, 1.2f));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            agent.speed = 0;
            other.gameObject.GetComponent<PlayerScript>().Stun();
            AudioManager.Instance.PlaySound(shock);
            foundPlayer = false;
            float c = 0;
            currentPosition = transform.position;
            while (c < 0.2)
            {
                Invoke("Glitch", c);
                c += 0.02f;
            }
            Invoke("Recover", 30f);
        }
    }

    //voltar a andar depois do tempo de delay
    private void ResetSpeed()
    {
        agent.speed = _speed;
    }

    //fun��o de glitchar a rota�ao e posi�ao
    private void Glitch()
    {
        transform.Rotate(0, Random.Range(0,360), 0);
        transform.position = currentPosition + new Vector3(Random.Range(-0.3f, 0.3f), 0, Random.Range(-0.3f, 0.3f));
    }

    //recover depois de dar sun no player
    private void Recover()
    {
        Invoke("ResetSpeed", 0);
        SetDestination();
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current)
    {
        if (current.gameStage == GameManager.GameStage.SecondStage)
        {
            if(!isEnemy)
                gameObject.SetActive(true);
        }
        if (current.gameStage == GameManager.GameStage.ThirdStage)
        {
            if (isEnemy)
                gameObject.SetActive(true);
            else
                gameObject.SetActive(false);
        }
    }
}
