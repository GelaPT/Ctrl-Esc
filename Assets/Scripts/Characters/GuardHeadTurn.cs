using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardHeadTurn : MonoBehaviour
{
    [SerializeField] public Transform lookTarget;

    [SerializeField] private GuardScript guard;

    public bool canlook = true;

    private void Start()
    {
        canlook = true;
    }
    void LateUpdate()
    {
        //handle se esta num raio de visao do guarda
        Vector3 toPosition = (lookTarget.position - transform.position).normalized;
        float angleToPosition = Vector3.Angle(transform.forward, toPosition);

        if(angleToPosition < 70)
        {
            if(canlook)
                transform.LookAt(lookTarget);
        }
        else
        {
            if (canlook)
                transform.rotation = guard.transform.rotation;
        }

    }
}
