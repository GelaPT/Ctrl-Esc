using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DeadMannequinScript : MonoBehaviour
{
    [SerializeField] private AudioClip spotPlayerClip;
    private AudioSource spotPlayerSource;
    [SerializeField] private LayerMask wallMask;
    [SerializeField] private LayerMask playerMask;

    [SerializeField] private GameObject[] path;
    private int pathIndex = -1;
    private int pathMultplier = 1;

    private Animator _animator;
    private NavMeshAgent agent;

    private Vector3 target;

    public bool foundPlayer;
    private GameObject player;
    public GameObject Player {
        get {
            return player;
        } set {
            OnPlayerSet();
            player = value;
        }
    }

    private float timer = 0;

    public float Speed = 1f;
    private float _speed;

    private float clampSpeed = 0.8f;

    [SerializeField] private MouseLook mouseLook;

    void Start()
    {
        _animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        
        agent.speed = Speed;
        _speed = agent.speed;

        target = transform.position; 

        if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.ThirdStage) {
            gameObject.SetActive(true);
        } else {
            gameObject.SetActive(false);
        }

        GameManager.Instance.onGameStateChanged.AddListener(GameStageChanged);
    }

    // Update is called once per frame
    void Update()
    {
        float animatorSpeed = agent.velocity.magnitude;

        if (animatorSpeed > clampSpeed)
            animatorSpeed = clampSpeed;

        _animator.SetFloat("Speed", animatorSpeed);
        if (!foundPlayer)
        {
            if (agent.remainingDistance == 0)
            {
                SetDestination();
            }
        }
        else //se ele achar o player, passa a sempre segui-lo
        {
            if(Player)
                target = Player.transform.position;
            agent.SetDestination(target);
            timer -= Time.deltaTime;
            //
            if (timer <= 0) //perder o player
            {
                clampSpeed = 0.8f;
                foundPlayer = false;
                Player = null;
                SetDestination();
            }
        }
    }

    private void OnPlayerSet() {
        timer = 10;
        if(!foundPlayer) {
            foundPlayer = true;
            spotPlayerSource = AudioManager.Instance.PlaySound(spotPlayerClip, false);
            Debug.Log("seen player");
            clampSpeed = 1;
            Invoke("ResetSpeed", 0f);
        }
    }

    private void FixedUpdate()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, transform.forward, out hit, 10f, wallMask))
        {
            if (Physics.SphereCast(transform.position, 2f, transform.forward, out hit, 10f, playerMask)) 
            {
                if(Player != hit.collider.gameObject) Player = hit.collider.gameObject;
            }
        }
    }

    private void SetDestination()
    {
        if (spotPlayerSource)
            AudioManager.Instance.StopSound(spotPlayerSource);
        if (pathIndex == path.Length && pathMultplier == 1)
        {
            pathMultplier = -1;
        } 
        else if (pathIndex < 0 && pathMultplier == -1)
        {
            pathMultplier = 1;
        }
        pathIndex += pathMultplier;
        agent.speed = 0.1f;
        target = path[pathIndex].transform.position;
        agent.SetDestination(target);
        Debug.Log(pathIndex);
        Invoke("ResetSpeed", Random.Range(0.7f, 1.2f));
    }

    //voltar a andar depois do tempo de delay
    private void ResetSpeed()
    {
        agent.speed = _speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {
            Time.timeScale = 0;
            other.gameObject.GetComponent<PlayerScript>().canMove = false;
            other.gameObject.GetComponent<PlayerScript>().Died();
            mouseLook.canLook = false;
            Cursor.lockState = CursorLockMode.Confined;
        }
    }

    private void GameStageChanged(GameManager.GameStates previous, GameManager.GameStates current) {
        if(current.gameStage == GameManager.GameStage.ThirdStage) {
            gameObject.SetActive(true);
        }
    }
}
