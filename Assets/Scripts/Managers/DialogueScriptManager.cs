using TMPro;
using UnityEngine;

public class DialogueScriptManager : Singleton<DialogueScriptManager>
{
    public GameObject subtitlePanel;
    public TextMeshProUGUI personName;
    public TextMeshProUGUI subtitle;
    public Color playercolor;
    public Color othercolor;

    public string[] lines;
}
