using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AmbientSound {
    public string audioName;
    public AudioClip audioClip;
    public bool loopSound;
    public Vector3 pos;
    public float maxDistance;
    [HideInInspector] public AudioSource audioSource;
}

public class AudioManager : Singleton<AudioManager> {
    public List<AmbientSound> ambientSounds = new List<AmbientSound>();
    public GameObject playerCam;
    public GameObject pointAudioPrefab;
    private AudioSource currentAmbSound;
    private List<AudioSource> audioSourcesAtLoc = new List<AudioSource>();

    private int lastVolume = -1;
    private void Start() {
        GameManager.Instance.onGameStateChanged.AddListener(PlayAmbientSounds);
    }
    private void Update() {
        if(!playerCam || ambientSounds.Count < 1) return;

        transform.position = playerCam.transform.position;
        transform.rotation = playerCam.transform.rotation;

        if(lastVolume != OptionsManager.Instance.Volume) {
            ChangeVolume();
        }
        lastVolume = OptionsManager.Instance.Volume;
    }

    public void PlayAmbientSounds(GameManager.GameStates previous, GameManager.GameStates current) {
        if(previous.gameState != current.gameState) return;
        if(ambientSounds.Count < 1) return;
        switch (current.gameStage) {
            case GameManager.GameStage.FirstStage:
                if(currentAmbSound) StopSound(currentAmbSound);
                currentAmbSound = PlaySound(ambientSounds[0].audioClip, true);
                break;
            case GameManager.GameStage.SecondStage:
                if(currentAmbSound) StopSound(currentAmbSound);
                currentAmbSound = PlaySound(ambientSounds[1].audioClip, true);
                break;
            case GameManager.GameStage.ThirdStage:
                if(currentAmbSound) StopSound(currentAmbSound);
                currentAmbSound = PlaySound(ambientSounds[2].audioClip, true);
                break;
        }
    }

    public void PlayAmbientSounds() {
        switch (GameManager.Instance.CurrentGameState.gameStage) {
            case GameManager.GameStage.FirstStage:
                currentAmbSound = PlaySound(ambientSounds[0].audioClip, true);
                break;
            case GameManager.GameStage.SecondStage:
                currentAmbSound = PlaySound(ambientSounds[1].audioClip, true);
                break;
            case GameManager.GameStage.ThirdStage:
                currentAmbSound = PlaySound(ambientSounds[2].audioClip, true);
                break;
        }
    }

    public void ChangeVolume() {
        foreach(AudioSource audioSource in GetComponents<AudioSource>()) {
            if(audioSource != null)  {
                audioSource.volume = OptionsManager.Instance.Volume / 10f;
            }
        }
    }

    public void ResetAudio() {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        foreach(AudioSource aS in audioSources) {
            if(aS)StopSound(aS);
        }

        foreach(AudioSource aS in audioSourcesAtLoc) {
            if(aS)StopSound(aS);
        }
        audioSourcesAtLoc.Clear();
    }

    public AudioSource PlaySound(AudioClip audioClip, bool loop = false) {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.volume = OptionsManager.Instance.Volume / 10f;
        audioSource.loop = loop;
        audioSource.clip = audioClip;
        audioSource.Play(0);
        if(!loop) Destroy(audioSource, audioClip.length);
        return audioSource;
    }

    public AudioSource PlaySoundAtLocation(AudioClip audioClip, Vector3 loc, float dis, bool loop = false) {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSourcesAtLoc.Add(audioSource);
        audioSource.volume = OptionsManager.Instance.Volume / 10f;
        audioSource.loop = loop;
        audioSource.clip = audioClip;
        audioSource.maxDistance = dis;
        AudioSource.PlayClipAtPoint(audioClip, loc, audioSource.volume);
        if(!loop) {
            Destroy(audioSource, audioClip.length);
            for(int i = 0; i < audioSourcesAtLoc.Count; i++) {
                if(audioSourcesAtLoc[i] == null) {
                    audioSourcesAtLoc.RemoveAt(i);
                }
            }
        }
        return audioSource;
    }

    public void StopSound(AudioSource audioSource) {
        if(audioSource) audioSource.Stop();
        if(audioSource) Destroy(audioSource);
    }
}