using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MonologueManager : Singleton<MonologueManager>
{
    public GameObject subtitlePanel;
    public TextMeshProUGUI personName;
    public TextMeshProUGUI subtitle;
    public GameObject notifPanel;
    public AudioClip notifClip;
    public Color playercolor;
    public Color othercolor;
    public Color viruscolor;

    [SerializeField] private AudioClip[] voices;

    private float timer = 10f;
    private bool _enabled;
    public void Update()
    {
        if (_enabled)
        {
            if (timer > 0)
            {
                timer -= 1 * Time.deltaTime;
            }
            else if (timer < 0)
            {
                subtitle.text = "";
                personName.text = "";
                subtitlePanel.SetActive(false);
                notifPanel.SetActive(false);
                _enabled = false;
            }
        }
    }
    public void ShowMonologue(string name, string speech)
    {
        _enabled = true;
        subtitlePanel.SetActive(true);
        switch (name)
        {
            case "namephil":
                subtitle.color = othercolor;
                personName.text = "Phil";
                break;
            case "player":
                subtitle.color = playercolor;
                personName.text = "Me";
                break;
            case "virus":
                subtitle.color = viruscolor;
                personName.text = "Virus";
                break;
        }
        StopCoroutine("PlayText");
        StartCoroutine("PlayText", speech);
        timer = 10f;
    }

    public void SpeakMonologue(int index)
    {
        AudioManager.Instance.PlaySound(voices[index]);
    }

    public void Notification()
    {
        Invoke("ShowNotification", 3);
    }
    IEnumerator PlayText(string line)
    {
        subtitle.text = "";
        foreach (char c in line)
        {
            Instance.subtitle.text += c;
            yield return new WaitForSeconds(0.05f);
        }
    }

    private void ShowNotification()
    {
        _enabled = true;
        timer = 5f;
        AudioManager.Instance.PlaySound(notifClip);
        notifPanel.SetActive(true);
    }
}
