﻿using UnityEngine;
using UnityEngine.UI;

#region Structs

[System.Serializable]
public struct MainMenu {
    public GameObject menuObject;
    public Button continueButton;
    public Button newGameButton;
    public Button optionsButton;
    public Button creditsButton;
    public Button exitButton;
}

[System.Serializable]
public struct PauseMenu {
    public GameObject menuObject;
}

#endregion Structs

public class UIManager : Singleton<UIManager> {
    public Camera UICamera;
    public MainMenu mainMenu;
    public PauseMenu pauseMenu;

    private void Start() {
        GameManager.Instance.onGameStateChanged.AddListener(OnGameStateChanged);
    }

    private void Pause() {
        pauseMenu.menuObject.SetActive(true);
    }

    private void UnPause() {
        pauseMenu.menuObject.SetActive(false);
    }
    
    private void OnGameStateChanged(GameManager.GameStates previous, GameManager.GameStates current) {
        if(previous.gameState == GameManager.GameState.Null || current.gameState == GameManager.GameState.Null)
        switch(current.gameState) {
            case GameManager.GameState.PreGame:
                UICamera.gameObject.SetActive(true);
                break;

            case GameManager.GameState.Running:
                UICamera.gameObject.SetActive(false);
                switch(previous.gameState) {
                    case GameManager.GameState.Paused:
                        UnPause();
                        break;
                }
                break;

            case GameManager.GameState.Paused:
                Pause();
                break;
        }
    }
}