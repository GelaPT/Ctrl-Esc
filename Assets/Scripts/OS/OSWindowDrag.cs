using UnityEngine;
using UnityEngine.EventSystems;

public class OSWindowDrag : MonoBehaviour, IDragHandler, IBeginDragHandler {
    [SerializeField] private RectTransform parent;
    private Canvas canvas;

    private Vector2 offset;

    private void Start() {
        canvas = TaskManager.Instance.currentTask.taskCanvas;
    }

    public void OnBeginDrag(PointerEventData ped) {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, ped.position, canvas.worldCamera, out pos);
        offset = parent.anchoredPosition - pos;
    }

    public void OnDrag(PointerEventData ped) {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, ped.position, canvas.worldCamera, out pos);
        if(!(canvas.transform as RectTransform).rect.Contains(pos)) return;
        parent.position = canvas.transform.TransformPoint(pos + offset);
        parent.SetAsLastSibling();
    }

    public void OnEndDrag(PointerEventData ped) {
        offset = new Vector3();
    }
}
