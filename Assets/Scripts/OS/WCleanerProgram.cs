using UnityEngine;
using UnityEngine.UI;

public class WCleanerProgram : MonoBehaviour {
    public Button button;
    public Text text;
    public Image image;
    [HideInInspector] public OSProgram program;

    private void Start() {
        button.onClick.AddListener(() => RemoveProgram());
    }

    private void RemoveProgram() {
        TaskManager.Instance.currentTask.osManager.RemoveProgram(program.programID);
        Destroy(gameObject);
    }
}