using UnityEngine;
using UnityEngine.UI;

public class OSDesktopProgram : MonoBehaviour {
    public Button button;
    public Text text;
    public Image image;
    [HideInInspector] public OSProgram program;

    private void Start() {
        button.onClick.AddListener(() => TaskManager.Instance.currentTask.osManager.OpenProgram(program.programID));
    }
}