using UnityEngine;
using UnityEngine.UI;

public class OSPeint : MonoBehaviour {
    public RenderTexture renderTex;
    public RawImage image;

    private Canvas canvas;

    private Texture2D texture;

    private Color[,] tex;

    private void Start() {
        canvas = GetComponentInParent<Canvas>();
        texture = new Texture2D(renderTex.width, renderTex.height);
        tex = new Color[renderTex.width, renderTex.height];

        image.texture = texture;
        for(int i = 0; i < renderTex.width; i++)
            for(int j = 0; j < renderTex.height; j++)
                tex[i, j] = Color.white;
    }

    private void Update() {
        if(Input.GetKey(KeyCode.Mouse0)) {
            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(image.rectTransform, Input.mousePosition, canvas.worldCamera, out pos);
            pos += new Vector2(27f, 25f);
            pos *= new Vector2(renderTex.width/54, renderTex.height/50);
            if(pos.x >= 0 && pos.y >= 0 && pos.x < renderTex.width && pos.y < renderTex.height)
                tex[(int)pos.x, (int)pos.y] = Color.black;
        }

        if(Input.GetKeyDown(KeyCode.Mouse1)) {
            for(int i = 0; i < renderTex.width; i++)
                for(int j = 0; j < renderTex.height; j++)
                    tex[i, j] = Color.white;
        }

        RenderTexture.active = renderTex;
        for(int i = 0; i < renderTex.width; i++)
            for(int j = 0; j < renderTex.height; j++)
                texture.SetPixel(i, j, tex[i, j]);
        texture.Apply();
        RenderTexture.active = null;
    }
}
