using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OSAntivirus : MonoBehaviour {
    [SerializeField] private Image currentImage;
    [SerializeField] private Sprite[] hasVirus;
    [SerializeField] private GameObject analyzeBar;

    [SerializeField] private Sprite[] popups;
    private List<GameObject> activePopups = new List<GameObject>();
    [SerializeField] private GameObject popupPrefab;

    private float lastPopupTime = 0.0f;
    private bool isAnalyzing;

    private float analyzing;
    [SerializeField] private GameObject analyzingObject;
    [SerializeField] private GameObject notSafeObject;
    [SerializeField] private GameObject failedObject;
    [SerializeField] private float analyzingDuration;

    private void Start() {
        //if stage == 1 | else if stage == 2
        currentImage.sprite = hasVirus[0];
    }

    private void Update() {
        if(isAnalyzing) {
            analyzing += Time.deltaTime / analyzingDuration;

            if(Time.time - lastPopupTime > 1f) {
                lastPopupTime = Time.time;
                GameObject popupObject = Instantiate(popupPrefab, TaskManager.Instance.currentTask.osManager.popupParent);
                activePopups.Add(popupObject);

                RectTransform popupTransform = popupObject.transform as RectTransform;
                popupTransform.anchoredPosition = new Vector2(Random.Range(-62f, 62f), Random.Range(-20f, 30f));

                Image popupImage = popupObject.GetComponent<Image>();
                if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.FirstStage) popupImage.sprite = popups[Random.Range(0, 12)];
                else if(GameManager.Instance.CurrentGameState.gameStage == GameManager.GameStage.SecondStage) popupImage.sprite = popups[Random.Range(12, 24)];
            }

            analyzeBar.transform.localScale = new Vector3(analyzing, 1, 1);

            for(int i = activePopups.Count - 1; i >= 0; i--) {
                if(!activePopups[i]) {
                    activePopups.RemoveAt(i);
                }
            }

            if(activePopups.Count > 4) {
                for(int i = 0; i < activePopups.Count; i++) {
                    Destroy(activePopups[i]);
                }
                activePopups.Clear();
                isAnalyzing = false;
                analyzing = 0;

                analyzingObject.SetActive(false);
                failedObject.SetActive(true);
            }

            if(analyzing >= 1) {
                TaskManager.Instance.CompleteTask();
            }
        }
    }

    public void Analyze() {
        isAnalyzing = true;
        notSafeObject.SetActive(false);
        failedObject.SetActive(false);
        analyzingObject.SetActive(true);
    }
}
