using UnityEngine;

public class OSWCleaner : MonoBehaviour {
    [SerializeField] private Transform WCleanerProgramsParent;
    [SerializeField] private GameObject WCleanerProgramObject;
    private float lastTurn = 0.75f;

    void Start() {
        foreach(OSProgram program in TaskManager.Instance.currentTask.osManager.initialComputerPrograms) {
            if(program.isImportant) continue;
            GameObject wProgramObject = Instantiate(WCleanerProgramObject, WCleanerProgramsParent);
            WCleanerProgram wProgram = wProgramObject.GetComponent<WCleanerProgram>();

            wProgram.program = program;
            wProgram.text.text = program.name;
            wProgram.image.sprite = program.image;
        }
    }

    private void Update() {
        if(GameManager.Instance.CurrentGameState.gameStage != GameManager.GameStage.SecondStage) return;
        lastTurn -= Time.deltaTime;
        if(lastTurn <= 0) {
            Transform child = WCleanerProgramsParent.GetChild(0);
            child.SetAsLastSibling();
            lastTurn = 0.75f;
        }
    }
}
