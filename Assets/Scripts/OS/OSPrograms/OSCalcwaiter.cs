using UnityEngine;
using UnityEngine.UI;

public class OSCalcwaiter : MonoBehaviour {
    [SerializeField] private Text input;
    [SerializeField] private Text history;

    private string rawInput;
    private bool dot;
    private string rawInputDot;
    private bool negative;

    private char op;

    private float number, result;

    private void Start() {
        ResetText();
    }

    public void AddOperation(string operation) {
        if(operation == ",") {
            if(!dot) dot = true;
            UpdateText();
        } else {
            op = operation.ToCharArray()[0];
            if(input.text != "0") number = float.Parse(input.text);
            history.text = number.ToString() + op;
            ClearInput();
        }
    }

    public void AddNumber(int num) {
        if(input.text.Length == 13 && !negative) return;
        else if(input.text.Length == 14 && negative) return;

        if(input.text == "0") input.text = "";

        if(dot) rawInputDot += num;
        else if(!(rawInput.Length == 0 && num == 0)) rawInput += num;

        UpdateText();
    }

    public void RemoveNumber() {
        if(dot) {
            if(rawInputDot.Length == 0) dot = !dot;
            else rawInputDot = rawInputDot.Remove(rawInputDot.Length - 1);
        } else {
            if(rawInput.Length == 0) 
            rawInput = rawInput.Remove(rawInput.Length - 1);
        }

        UpdateText();
    }

    private void UpdateText() {
        input.text = "";

        if(negative && (rawInput.Length > 0 || rawInputDot.Length > 0)) input.text += "-";

        if(rawInput.Length == 0) {
            input.text += "0";
        } else {
            if(rawInput.Length > 3) {
                for(int i = 0; i < rawInput.Length; i++) {
                    if(i != 0 && (rawInput.Length - i) % 3 == 0) input.text += " ";
                    input.text += rawInput[i];
                }
            } else {
                input.text += rawInput;
            }
        }

        if(!dot) return;
        
        input.text += ',';
        input.text += rawInputDot;
    }

    public void ClearInput() {
        rawInput = "";
        rawInputDot = "";
        negative = false;
        dot = false;
        UpdateText();
    }

    public void ResetText() {
        ClearInput();
        number = default(float);
        op = default(char);
        history.text = "";
    }

    public void Calculate() {
        if(input.text == "0" || history.text.Length < 1) return;
        switch(op) {
            case '+':
                result = number + float.Parse(input.text);
                break;
            case '-':
                result = number - float.Parse(input.text);
                break;
            case 'x':
                result = number * float.Parse(input.text);
                break;
            case '/':
                result = number / float.Parse(input.text);
                break;
        }

        ResetText();
        input.text = result.ToString();
    }

    public void InvertNumber() {
        negative = !negative;
        UpdateText();
    }
}