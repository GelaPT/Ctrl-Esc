using UnityEngine;
using UnityEngine.UI;

public class OSColdfox : MonoBehaviour {
    [SerializeField] private InputField iField;

    public void OnValueChanged() {
        TaskManager.Instance.currentTask.osManager.isWriting = true;
    }

    public void OnValueEnded() {
        TaskManager.Instance.currentTask.osManager.isWriting = false;
        iField.text = "";
    }
}
