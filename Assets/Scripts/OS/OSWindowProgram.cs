using UnityEngine;
using UnityEngine.UI;

public class OSWindowProgram : MonoBehaviour {
    public Button button;
    public Text text;
    public Image image;
    [HideInInspector] public OSProgram program;

    private void Start() {
        button.onClick.AddListener(() => TaskManager.Instance.currentTask.osManager.CloseProgram(program.programID));
    }
}
