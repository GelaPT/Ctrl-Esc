using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

[System.Serializable]
public class OSProgram {
    public string programID;
    public string name;
    public Sprite image;
    public GameObject window;
    public bool isImportant;
    [HideInInspector] public bool isOpen;
    [HideInInspector] public GameObject openObject;
    [HideInInspector] public GameObject openTaskObject;
    [HideInInspector] public GameObject startMenuObject;
    [HideInInspector] public GameObject desktopObject;
}

public class OSManager : MonoBehaviour, IPointerClickHandler {

    #region OSManagerVars
    [SerializeField] private Text timeOfDay;
    [SerializeField] private Sprite[] wallpapers;
    [SerializeField] private Image wallpaper;
    [SerializeField] private GameObject sticky;
    [SerializeField] private Text[] stickyText;
    #endregion

    #region StartMenuVars
    [Header("Start Menu")]
    [SerializeField] private GameObject startMenu;
    private bool onStartMenu;

    [SerializeField] private Sprite[] avatars;
    [SerializeField] private Image avatar;
    #endregion

    #region ProgramVars
    [Header("Programs")]
    [SerializeField] private Transform openProgramsParent;
    [SerializeField] private Transform startMenuParent;
    [SerializeField] private Transform desktopParent;
    [SerializeField] private GameObject startMenuPrefab;
    [SerializeField] private GameObject desktopPrefab;
    [SerializeField] private List<OSProgram> programs;
    [HideInInspector] public List<OSProgram> computerPrograms = new List<OSProgram>();
    [HideInInspector] public List<OSProgram> initialComputerPrograms = new List<OSProgram>();
    [HideInInspector] private List<string> removingPrograms = new List<string>();
    public bool isWriting;
    #endregion

    #region TaskVars
    public Transform popupParent;
    [HideInInspector] public bool antivirusTask;
    [SerializeField] private AudioClip taskClick;
    [SerializeField] private AudioClip openAudio;
    [SerializeField] private AudioClip loopAudio;
    [SerializeField] private AudioClip closeAudio;
    private AudioSource loopAudioSource;
    [SerializeField]private AudioListener audioList;
    private float lastBackgroundFlip = 0;
    #endregion

    #region OSManager

    private void Update() {
        OSTimeOfDay();
        if(GameManager.Instance.CurrentGameState.gameStage != GameManager.GameStage.SecondStage || !OptionsManager.Instance.CameraMotion) return;
        if(Time.time - lastBackgroundFlip > .5f) {
            Sprite newSprite = wallpapers[UnityEngine.Random.Range(5, 10)];
            while(wallpaper.sprite == newSprite) {
                newSprite = wallpapers[UnityEngine.Random.Range(5, 10)];
            }
            wallpaper.sprite = newSprite;
            lastBackgroundFlip = Time.time;
        }
    }

    private void OnEnable() {
        audioList.enabled = true;
        AudioManager.Instance.PlaySound(openAudio);
        loopAudioSource = AudioManager.Instance.PlaySound(loopAudio, true);
    }

    private void OnDisable() {
        audioList.enabled = false;
        if(loopAudioSource) AudioManager.Instance.StopSound(loopAudioSource);
        OSEnd();
    }

    private void OSTimeOfDay() {
        DateTime now = DateTime.Now;
        now = now.AddYears(20);

        timeOfDay.text = now.ToString("HH:mm\ndd/MM/yyyy");
    }

    private void Start() {
        if(antivirusTask) {
            AddProgram("antivirus", true);
        } else {
            sticky.SetActive(true);
            AddProgram("wcleaner", true);
        }

        if(!wallpaper.sprite) wallpaper.sprite = wallpapers[UnityEngine.Random.Range(0, 5)];
        if(!avatar.sprite) avatar.sprite = avatars[UnityEngine.Random.Range(0, avatars.Length)];
        if(computerPrograms.Count < 2) {
            foreach(OSProgram program in programs)
                if(!program.isImportant)
                    AddProgram(program.programID);

        }

        initialComputerPrograms = computerPrograms;

        if(!antivirusTask) {
            int removingCount = UnityEngine.Random.Range(2, 4);
            int r = 0;
            while(r < removingCount) {
                foreach(OSProgram program in initialComputerPrograms) {
                    if(!program.isImportant && !removingPrograms.Contains(program.programID)) {
                        if(UnityEngine.Random.Range(0, 3) == 0 && r < removingCount) {
                            removingPrograms.Add(program.programID);
                            stickyText[r++].text = program.name;
                        }
                    }
                }
            }
        }

        OSTimeOfDay();

        // Start Menu
        onStartMenu = false;
        startMenu.SetActive(false);
    }

    private void OSEnd() {
        foreach(OSProgram program in computerPrograms)
            if(program.isOpen)
                CloseProgram(program.programID);
    }

    private void OSReset() {
        for(int i = computerPrograms.Count - 1; i > -1; i--) {
            RemoveProgram(computerPrograms[i].programID, false);
        }

        computerPrograms.Clear();

        AddProgram("wcleaner", true);

        foreach(OSProgram program in programs)
            if(!program.isImportant)
                AddProgram(program.programID);

        onStartMenu = false;
        startMenu.SetActive(false);

        OSExit();
    }

    public void OSExit() {
        AudioManager.Instance.PlaySound(closeAudio);
        TaskManager.Instance.LeaveTask();
        audioList.enabled = false;
        if(loopAudioSource) AudioManager.Instance.StopSound(loopAudioSource);
    }

    #endregion

    #region StartMenu

    public void ToggleStartMenu() {
        onStartMenu = !onStartMenu;
        startMenu.SetActive(onStartMenu);
    }
    
    public void SetStartMenu(bool value) {
        onStartMenu = value;
        startMenu.SetActive(onStartMenu);
    }

    #endregion

    #region Programs
    private OSProgram TryFindProgram(string programID, bool inComputer) {
        if(inComputer) {
            if(computerPrograms.Count < 1) return null;

            foreach(OSProgram program in computerPrograms)
                if(program.programID == programID) return program;

            return null;
        }

        if(programs.Count < 1) return null;

        foreach(OSProgram program in programs)
            if(program.programID == programID) return program;

        return null;
    }

    public bool AddProgram(string programID, bool desktop = false) {
        OSProgram program;
        if((program = TryFindProgram(programID, false)) == null) return false;
        if(TryFindProgram(programID, true) != null) return false;

        computerPrograms.Add(program);

        program.startMenuObject = Instantiate(startMenuPrefab, startMenuParent);
        OSStartMenuProgram smp = program.startMenuObject.GetComponent<OSStartMenuProgram>();

        smp.text.text = program.name;
        smp.image.sprite = program.image;
        smp.program = program;

        if(desktop) {
            program.desktopObject = Instantiate(desktopPrefab, desktopParent);

            OSDesktopProgram dp = program.desktopObject.GetComponent<OSDesktopProgram>();

            dp.text.text = program.name;
            dp.image.sprite = program.image;
            dp.program = program;
        } else if(UnityEngine.Random.Range(0, 2) == 0) {
            program.desktopObject = Instantiate(desktopPrefab, desktopParent);

            OSDesktopProgram dp = program.desktopObject.GetComponent<OSDesktopProgram>();

            dp.text.text = program.name;
            dp.image.sprite = program.image;
            dp.program = program;
        }

        return true;
    }

    public void RemoveProgram(string programID, bool checkRemove = true) {
        AudioManager.Instance.PlaySound(taskClick);
        if(checkRemove) {
            if(!removingPrograms.Contains(programID)) {
                OSReset();
                return;
            }
        }

        OSProgram program;
        if((program = TryFindProgram(programID, true)) == null) return;

        CloseProgram(programID);

        Destroy(program.startMenuObject);
        if(program.desktopObject) Destroy(program.desktopObject);
        for(int i = 0; i < computerPrograms.Count; i++) {
            if(computerPrograms[i].programID == programID) {
                computerPrograms.RemoveAt(i);
            }
        }

        if(!checkRemove) return;

        foreach(OSProgram osprogram in computerPrograms) {
            if(removingPrograms.Contains(osprogram.programID)) {
                return;
            }
        }

        TaskManager.Instance.CompleteTask();
    }

    public void OpenProgram(string programID) {
        AudioManager.Instance.PlaySound(taskClick);
        OSProgram program;
        if((program = TryFindProgram(programID, true)) == null) return;
        if(program.isOpen) {
            program.openObject.transform.SetAsLastSibling();
            program.openObject.transform.localPosition = new Vector3(0, 0, 0);
            return;
        }

        program.isOpen = true;

        program.openObject = Instantiate(program.window, openProgramsParent);
        program.openObject.transform.SetAsLastSibling();
        OSWindowProgram window = program.openObject.GetComponent<OSWindowProgram>();

        window.text.text = program.name;
        window.image.sprite = program.image;
        window.program = program;
    }

    public void CloseProgram(string programID) {
        AudioManager.Instance.PlaySound(taskClick);
        OSProgram program;
        if((program = TryFindProgram(programID, true)) == null) return;
        if(!program.isOpen) return;

        program.isOpen = false;
        isWriting = false;
        Destroy(program.openObject);
    }
    #endregion

    #region OnClick
    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);

        if(results.Count > 0) {
            if(results[0].gameObject.layer == LayerMask.NameToLayer("WorldUI")) {
                if(onStartMenu) {
                    if(!results[0].gameObject.CompareTag("Start Menu")) {
                        SetStartMenu(false);
                    }
                }

                results.Clear();
            }
        }
    }
    #endregion
}