using UnityEngine;

public class Popup : MonoBehaviour {
    [SerializeField] private AudioClip click;
    public void ClosePopup() {
        AudioManager.Instance.PlaySound(click);
        Destroy(gameObject);
    }
}